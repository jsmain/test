import { FC, useEffect, useMemo } from 'react';
import { Trans } from 'react-i18next';
import { Icon } from 'app/common/Icon';
import { FormHeader } from 'app/common/Form/FormHeader';
import { FormStepper } from 'app/common/Form/FormStepper';
import { CascoVehicle } from 'app/components/Insurance/Casco/CascoVehicle';
import { CascoClient } from 'app/components/Insurance/Casco/CascoClient';
import { CascoConclude } from 'app/components/Insurance/Casco/CascoConclude';
import { InsuranceCascoDto } from 'app/dto/insurance/casco';
import { InsuranceWizard } from 'app/components/Insurance/Wizard';
import { useDispatch, useSelector } from 'app/store/utils';
import { contactSelector } from 'app/features/auth/authSelectors';
import { InsuranceType } from 'app/dto/insurance';
import { useMeta } from 'hooks/useMeta';
import { ClientDataManager } from 'app/components/Insurance/ClientDataManager';
import { MetaTags } from 'app/common/MetaTags';
import { beginCheckout } from 'helpers/datalayer';
import { CascoPrices } from 'app/components/Insurance/Casco/CascoPrices';
import useMobile from 'hooks/useMobile';

interface InsuranceCascoProps {}

export const InsuranceCasco: FC<InsuranceCascoProps> = () => {
  const tags = useMeta('insurance.casco');
  const dispatch = useDispatch();
  const isMobile = useMobile();
  const initialValues = useMemo(
    () => ({
      data: {
        rangeOfUse: 'common_use',
        compensationVAT: false,
        indemnityLimitForAccessories: 2000,
        frontAndCabinWindows: true,
        cascoDailyAllowance: false,
      },
      client: {
        legalPerson: false,
      },
    }),
    []
  );
  const role = useSelector(contactSelector);
  const values: RecursivePartial<InsuranceCascoDto> | undefined = useMemo(
    () =>
      role && {
        client: {
          ...role,
          legalPerson: Boolean(role?.companyName),
          companyCode: role?.registerCode,
        },
      },
    [role]
  );

  useEffect(() => {
    beginCheckout('casco_insurance');
  }, [dispatch]);
  return (
    <InsuranceWizard
      name={InsuranceType.casco}
      values={initialValues}
      render={({ wizard: { step, changeStep } }) => (
        <>
          <MetaTags {...tags} />
          <ClientDataManager step={step} values={values} />
          <FormHeader
            icon={<Icon name="insurance-casco" size={isMobile ? 42 : 64} />}
            title={
              <Trans
                i18nKey="insurance.form.casco.title"
                defaults="Casco insurance"
              />
            }
          />
          <FormStepper step={step} onChange={changeStep}>
            <Trans
              i18nKey="insurance.form.casco.step.vehicle"
              defaults="Vehicle data"
            />
            <Trans
              i18nKey="insurance.form.casco.step.choose"
              defaults="Choose coverages"
            />
            <Trans
              i18nKey="insurance.form.casco.step.client"
              defaults="Client data"
            />
            <Trans
              i18nKey="insurance.form.casco.step.payment"
              defaults="Payment of insurance premium"
            />
            <Trans i18nKey="insurance.form.casco.step.done" defaults="Done" />
          </FormStepper>
        </>
      )}
    >
      <CascoVehicle />
      <CascoPrices />
      <CascoClient />
      <CascoConclude />
    </InsuranceWizard>
  );
};

export default InsuranceCasco;
