import { isValidElement, useCallback, useMemo } from 'react';
import Autocomplete, {
  AutocompleteProps as MuiAutocompleteProps,
} from '@mui/material/Autocomplete';
import { FormControl, TextField } from '@mui/material';
import { Field, UseFieldConfig } from 'react-final-form';
import {
  FinalFieldAdapterProps,
  getTextFieldMetaProps,
  InputElement,
} from 'fields/TextField';
import { Option } from 'app/dto/field';
import _isEqual from 'lodash/isEqual';
import { useTranslation } from 'react-i18next';
import { AutocompleteRenderInputParams } from '@mui/material/Autocomplete/Autocomplete';
import { renderToString } from 'react-dom/server';
import FormHelperText from '@mui/material/FormHelperText';

export type AutocompleteProps<
  T,
  O extends Option<T> = Option<T>,
  Multiple extends boolean | undefined = false,
  DisableClearable extends boolean | undefined = true,
  FreeSolo extends boolean | undefined = false
> = MuiAutocompleteProps<O, Multiple, DisableClearable, FreeSolo>;

export interface SelectFieldProps<T>
  extends Omit<AutocompleteProps<T>, 'renderInput'> {
  name: string;
  displayEmpty?: boolean;
  renderInput?: AutocompleteProps<T>['renderInput'];
}

export type SelectFieldAdapterProps<T> = FinalFieldAdapterProps<T> &
  Partial<SelectFieldProps<T>>;

export function SelectFieldAdapter<T>(props: SelectFieldAdapterProps<T>) {
  const {
    input,
    meta,
    options: propsOptions,
    onChange,
    displayEmpty,
    ...field
  } = props;

  const { t } = useTranslation();
  const options = useMemo(
    () =>
      [
        (displayEmpty && {
          value: '',
          label: t('select.option.none', { defaultValue: 'None' }),
        }) as unknown as Option<T>,
        ...(propsOptions ?? []).map(({ value, label }) => ({
          value,
          label: isValidElement(label) ? renderToString(label) : label,
        })),
      ].filter(Boolean),
    [displayEmpty, propsOptions, t]
  );

  type Props = AutocompleteProps<T>;
  const { value: inputValue } = input;
  const value = useMemo<Option<T> | null>(
    () => options.find(({ value }) => _isEqual(value, inputValue)) ?? null,
    [inputValue, options]
  );

  const { helperText, ...metaProps } = getTextFieldMetaProps(meta);

  const renderInput = useCallback(
    (params: AutocompleteRenderInputParams) => (
      <TextField
        {...params}
        {...metaProps}
        onFocus={input.onFocus}
        onBlur={input.onBlur}
        inputProps={{
          role: 'presentation',
          autoComplete: 'none',
          placeholder: t('select.placeholder', {
            defaultValue: 'Please choose',
          }),
          ...params.inputProps,
        }}
      />
    ),
    [input.onBlur, input.onFocus, metaProps, t]
  );
  const renderOption: Props['renderOption'] = useCallback(
    (props, option) => (
      <li {...props} key={props.id}>
        {option.label}
      </li>
    ),
    []
  );

  const handleChange: Props['onChange'] = useCallback(
    (event, option, reason, details) => {
      onChange?.(event, option, reason, details);
      input.onChange(option.value);
    },
    [input, onChange]
  );

  const isOptionEqualToValue: Props['isOptionEqualToValue'] = useCallback(
    (option, { value }) => {
      return _isEqual(option.value, value);
    },
    []
  );

  return (
    <FormControl fullWidth {...metaProps}>
      <Autocomplete
        renderInput={renderInput}
        renderOption={renderOption}
        options={options!}
        // onOpen={handleOpen}
        onChange={handleChange}
        value={value!}
        isOptionEqualToValue={isOptionEqualToValue}
        disableClearable
        blurOnSelect
        {...field}
      />
      {Boolean(helperText) && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
}

export function SelectField<T>(props: SelectFieldProps<T> & UseFieldConfig<T>) {
  return (
    <Field<T, InputElement, T> {...props} component={SelectFieldAdapter} />
  );
}
