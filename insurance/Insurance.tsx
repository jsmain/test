import { FC, useCallback, useEffect } from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { InsuranceType } from 'app/dto/insurance';
import { InsuranceTest } from 'app/containers/Insurance/Test';
import { getContent } from 'app/features/drupal/drupalActions';
import { useDispatch } from 'app/store/utils';
import loadable from '@loadable/component';
import { useLocation } from 'react-router';
import qs from 'qs';

const NotFound = loadable(() => import('app/containers/NotFound'));
const InsuranceTraffic = loadable(
  () => import('app/containers/Insurance/Traffic')
);
const InsuranceHome = loadable(() => import('app/containers/Insurance/Home'));
const InsuranceAccident = loadable(
  () => import('app/containers/Insurance/Accident')
);
const InsuranceTravel = loadable(
  () => import('app/containers/Insurance/Travel')
);
const InsuranceCasco = loadable(() => import('app/containers/Insurance/Casco'));
const InsuranceApartment = loadable(
  () => import('app/containers/Insurance/Apartment')
);
const InsuranceDraft = loadable(() => import('app/containers/InsuranceDraft'));

const { traffic, home, accident, travel, casco, apartment } = InsuranceType;

const Insurance: FC<RouteComponentProps> = ({ match }) => {
  // const content = useCallback(async () => {
  //   await dispatch(getContent({pathname, search})).unwrap();
  // }, [dispatch, pathname, search])
  const dispatch = useDispatch();
  const path = useCallback(
    (type: InsuranceType) => `${match.path}/${type}`,
    [match]
  );
  const location = useLocation<{ ref?: string }>();
  const { ref } = qs.parse(location.search, { ignoreQueryPrefix: true });

  useEffect(() => {
    if (ref) return;
    dispatch(getContent({ pathname: '/', search: '/' })).unwrap();
  }, [dispatch, ref]);
  return (
    <Switch>
      {process.env.NODE_ENV === 'development' && (
        <Route
          path={path(InsuranceType.test)}
          component={InsuranceTest}
          exact
        />
      )}
      <Route path={path(traffic)} component={InsuranceTraffic} exact />
      <Route path={path(home)} component={InsuranceHome} exact />
      <Route path={path(accident)} component={InsuranceAccident} exact />
      <Route path={path(travel)} component={InsuranceTravel} exact />
      <Route path={path(casco)} component={InsuranceCasco} exact />
      <Route path={path(apartment)} component={InsuranceApartment} exact />
      <Route
        path={`${match.path}/:type(${traffic}|${casco})/draft/:uuid`}
        component={InsuranceDraft}
        exact
      />
      <Route path="*" component={NotFound} />
    </Switch>
  );
};

export default Insurance;
