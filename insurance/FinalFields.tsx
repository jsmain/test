import {
  CSSProperties,
  PropsWithChildren,
  ReactElement,
  ReactNode,
} from 'react';
import { ConditionRenderer, ConditionFieldProp } from 'fields/ConditionField';
import { FieldKeys, TransField } from 'i18n/trans/field';
import {
  Field,
  FieldRenderProps,
  FormSpy,
  FormSpyRenderProps,
  UseFieldConfig,
} from 'react-final-form';
import { Option } from 'app/dto/field';
import { FieldValidator } from 'final-form';
import { LabelField, LabelFieldTooltip } from 'fields/LabelField';
import { GridListItem } from 'app/common/GridList';
import { SelectField, SelectFieldProps } from 'fields/SelectField';
import { TextField, TextFieldProps } from 'fields/TextField';
import { CheckboxField } from 'fields/CheckboxField';
import { Tooltip } from 'mui/components/Tooltip';
import { RadioGroupField } from 'fields/RadioGroupField';
import { DateField, DateFieldProps } from 'fields/DateField';
import { Divider } from '@mui/material';
import { boolFieldLabel } from 'helpers/field';
import { AddressField } from 'fields/AddressField';
import { MaskedField, MaskedFieldProps } from 'fields/MaskedField';

type FieldType =
  | 'field'
  | 'checkbox'
  | 'email'
  | 'date'
  | 'number'
  | 'radio'
  | 'tel'
  | 'textarea'
  | 'text'
  | 'address'
  | 'select'
  | undefined;

interface BaseFieldOptions {
  key?: string;
  name?: string;
  style?: CSSProperties;
  labelStyle?: CSSProperties;
  fieldStyle?: CSSProperties;
  fullWidth?: boolean;
  placeholder?: string;
  i18n?: FieldKeys;
  label?: ReactElement | FieldKeys;
  required?: boolean;
  disabled?: boolean;
  checked?: boolean;
  tooltip?: ReactElement;
  condition?: ConditionFieldProp;
}

interface BaseFormFieldOptions<
  Values extends FieldValues,
  Name extends FieldPath<Values>,
  Value extends UnpackNestedValue<
    FieldPathValue<Values, Name>
  > = UnpackNestedValue<FieldPathValue<Values, Name>>
> extends BaseFieldOptions,
    Pick<UseFieldConfig<Value>, 'parse' | 'format'> {
  name: Name;
  accessor?: Name;
  validate?: FieldValidator<Value>;
  render?: (props: FieldRenderProps<Value>) => ReactNode;
  defaults?: ReactNode;
}

type TypedField<
  Type extends FieldType,
  Values extends FieldValues,
  Name extends FieldPath<Values>,
  Value extends UnpackNestedValue<
    FieldPathValue<Values, Name>
  > = UnpackNestedValue<FieldPathValue<Values, Name>>
> = Type extends infer R
  ? R extends 'text' | 'textarea' | 'tel' | 'number' | 'email'
    ? {
        type: R;
        onChange?: TextFieldProps['onChange'];
        multiline?: TextFieldProps['multiline'];
        autoComplete?: string | undefined;
        // mask?: string | Array<string | RegExp>;
        mask?: MaskedFieldProps['mask'];
      }
    : R extends 'checkbox'
    ? {
        type: 'checkbox';
      }
    : R extends 'date'
    ? {
        type: 'date';
      } & DateFieldProps
    : R extends 'radio'
    ? {
        type: 'radio';
        options: Array<Option<Value>>;
      }
    : R extends 'select'
    ? {
        type: 'select';
      } & SelectFieldProps<Value>
    : R extends 'address'
    ? {
        type: 'address';
        displayEmpty?: boolean;
        noOptionsText?: ReactNode;
      }
    : {
        type?: R;
      }
  : never;

type FormFieldOptions<
  Values extends FieldValues,
  Name extends FieldPath<Values>,
  Type extends FieldType
> = BaseFormFieldOptions<Values, Name> & TypedField<Type, Values, Name>;

type FinalField<
  Values extends FieldValues,
  Name extends FieldPath<Values>,
  Type extends FieldType = FieldType
> = Name extends infer R
  ? R extends Name
    ? FormFieldOptions<Values, R, Type>
    : undefined
  : undefined;

interface SpyField<TValues extends FieldValues> extends BaseFieldOptions {
  key: string;
  type: 'spy';
  render: (props: FormSpyRenderProps<TValues>) => ReactNode;
}

interface ComponentField<TValues extends FieldValues> extends BaseFieldOptions {
  key: string;
  type: 'component';
  component?: Array<ReactNode> | ReactNode;
  render?: (props: FormSpyRenderProps<TValues>) => Array<ReactNode> | ReactNode;
}
interface DividerField extends BaseFieldOptions {
  key: string;
  type: 'divider';
}

const LabelRenderer = ({
  name,
  tooltip,
  labelStyle,
  children,
}: PropsWithChildren<
  Pick<BaseFieldOptions, 'name' | 'tooltip' | 'labelStyle'>
>) =>
  tooltip ? (
    <LabelFieldTooltip name={name} tooltip={tooltip} labelStyle={labelStyle}>
      {children}
    </LabelFieldTooltip>
  ) : (
    <LabelField name={name} labelStyle={labelStyle}>
      {children}
    </LabelField>
  );

export const checkboxRenderer = <
  TValues extends FieldValues,
  TName extends FieldPath<TValues> = FieldPath<TValues>,
  TValue = UnpackNestedValue<FieldPathValue<TValues, TName>>
>({
  input: { value },
  defaults,
}: FieldRenderProps<TValue>) => boolFieldLabel(Boolean(value), defaults);

type GetFieldType<T> = T extends { type: unknown } ? T['type'] : undefined;

type PossibleField<
  T extends FieldValues,
  K extends FieldPath<T>,
  F extends FinalField<T, K> | SpyField<T> | ComponentField<T> | DividerField,
  Type = GetFieldType<F>
> = Type extends FieldType
  ? FinalField<T, K, Type>
  : Type extends 'spy'
  ? SpyField<T>
  : Type extends 'component'
  ? ComponentField<T>
  : Type extends 'divider'
  ? DividerField
  : never;

export function finalFields<T extends FieldValues>(
  fields: Array<
    PossibleField<
      T,
      FieldPath<T>,
      | FinalField<T, FieldPath<T>>
      | SpyField<T>
      | ComponentField<T>
      | DividerField
    >
  >
) {
  return fields.map((props) => {
    const {
      type,
      key,
      i18n,
      tooltip,
      condition,
      required,
      style,
      labelStyle,
      fieldStyle,
      fullWidth = true,
      placeholder = '',
      ...rest
    } = props;
    const { name } = rest;
    const label = (({ label }) =>
      type !== 'checkbox' ? (
        (label && (
          <LabelRenderer name={name} tooltip={tooltip} labelStyle={labelStyle}>
            {label}
          </LabelRenderer>
        )) ??
        (i18n && (
          <LabelRenderer name={name} tooltip={tooltip} labelStyle={labelStyle}>
            <TransField i18nKey={i18n!} />
          </LabelRenderer>
        ))
      ) : typeof label === 'string' ? (
        <LabelRenderer name={name} tooltip={tooltip} labelStyle={labelStyle}>
          <TransField i18nKey={label} />
        </LabelRenderer>
      ) : (
        label
      ))(props);
    const field = (() => {
      switch (props.type) {
        case 'component':
          if (props.render) {
            return <FormSpy render={props.render} />;
          }
          return props.component ?? null;
        case 'divider':
          return <Divider sx={{ flex: 1, mt: 2, mb: 2 }} />;
        case 'spy':
          return <FormSpy render={props.render} />;
        default: {
          const fieldProps = { ...rest, name: props.name };
          switch (props.type) {
            case 'checkbox':
              return (
                <CheckboxField
                  {...fieldProps}
                  required={required}
                  labelStyle={fieldStyle}
                  label={
                    i18n ? (
                      <>
                        <TransField i18nKey={i18n} />
                        {tooltip && <Tooltip title={tooltip} />}
                      </>
                    ) : (
                      ''
                    )
                  }
                />
              );
            case 'text':
            case 'textarea':
            case 'email':
            case 'number':
            case 'tel':
              if (props.mask) {
                return (
                  <MaskedField
                    type={type}
                    placeholder={placeholder}
                    {...fieldProps}
                    mask={props.mask}
                    validate={props.validate}
                    fullWidth={fullWidth}
                    sx={fieldStyle}
                  />
                );
              }
              return (
                <TextField
                  {...fieldProps}
                  type={type}
                  fullWidth={fullWidth}
                  placeholder={placeholder}
                  sx={fieldStyle}
                />
              );
            case 'address':
              return <AddressField {...fieldProps} sx={fieldStyle} />;
            case 'select':
              return (
                <SelectField
                  {...fieldProps}
                  options={props.options!}
                  sx={fieldStyle}
                />
              );
            case 'radio':
              return (
                <RadioGroupField {...fieldProps} options={props.options!} row />
              );
            case 'date':
              return <DateField {...fieldProps} style={fieldStyle} />;
            default: {
              const {
                name,
                defaults,
                render = ({ input: { value } }: FieldRenderProps<string>) =>
                  value,
              } = props;
              return (
                <Field
                  name={name}
                  defaults={defaults}
                  render={(props) => render(props) || defaults}
                />
              );
            }
          }
        }
      }
    })();

    return (
      <ConditionRenderer key={key ?? name} condition={condition}>
        {type === 'divider' || (type === 'component' && props.render) ? (
          field
        ) : (
          <GridListItem required={required} sx={style}>
            {label}
            {field}
          </GridListItem>
        )}
      </ConditionRenderer>
    );
  });
}
