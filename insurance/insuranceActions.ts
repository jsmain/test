import { createAsyncThunk } from 'app/store/utils';
import { api } from 'app/features/api';
import { Policies, Policy } from 'app/dto/policy';
import qs from 'qs';
import { createAction } from '@reduxjs/toolkit';

export interface GetPoliciesOptions {
  page: number;
  size: number;
}

export interface GetPoliciesOptionsReturnType {
  query: GetPoliciesOptions;
  data: Policies;
}

export const getPoliciesDefaultOptions: GetPoliciesOptions = {
  page: 1,
  size: 500,
};

export const getPolicies = createAsyncThunk<
  GetPoliciesOptionsReturnType,
  { expired?: boolean } & Partial<GetPoliciesOptions>
>('insurance/getPolicies', async ({ expired, ...options } = {}) => {
  const query = { ...getPoliciesDefaultOptions, ...options };
  const { data } = await api.get(
    `/${['policies', expired && 'expired']
      .filter(Boolean)
      .join('/')}${qs.stringify(query, { addQueryPrefix: true })}`
  );
  return { query, data };
});

export const getPolicy = createAsyncThunk<Policy, string>(
  'insurance/getPolicy',
  async (id) => (await api.get(`/policies/${id}`)).data
);

export const clearPolicy = createAction('clear_policy');
