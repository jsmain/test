import { Switch, Route } from 'react-router-dom';
import { PrivateRoute } from 'app/PrivateRoute';
import loadable from '@loadable/component';

const Drupal = loadable(() => import('app/containers/Drupal'));
const Insurance = loadable(() => import('app/containers/Insurance'));
const PolicyContactForm = loadable(
  () => import('app/components/Policies/PolicyContractForm')
);
const Role = loadable(() => import('app/containers/Role'));
const Dashboard = loadable(() => import('app/containers/Dashboard'));
const Policies = loadable(() => import('app/containers/Policies'));
const Contacts = loadable(() => import('app/containers/Contacts'));
const Invoices = loadable(() => import('app/containers/Invoices'));
const Claims = loadable(() => import('app/containers/Claims'));
const ClaimDetails = loadable(() => import('app/containers/ClaimDetails'));
const Reports = loadable(() => import('app/containers/Reports'));
const Authorisations = loadable(() => import('app/containers/Authorisations'));
const FileShare = loadable(() => import('app/containers/FileShare'));
const InvoicesPayment = loadable(
  () => import('app/containers/Invoices/InvoicesPayment')
);

export const Routes = () => (
  <Switch>
    <Route path="/24/insurance" component={Insurance} />
    <Route
      path="/e-salva/form-changeterminate-insurance-contract"
      component={PolicyContactForm}
      exact
    />
    <PrivateRoute path="/role" component={Role} exact sidebar={false} />
    <PrivateRoute path="/dashboard" component={Dashboard} exact />
    <PrivateRoute path="/insurances" component={Policies} />
    <PrivateRoute path="/contacts" component={Contacts} exact />
    <PrivateRoute path="/invoices" component={Invoices} exact />
    <PrivateRoute path="/claims" component={Claims} exact />
    <PrivateRoute path="/claims/:id" component={ClaimDetails} exact />
    <PrivateRoute path="/reports" component={Reports} exact />
    <PrivateRoute path="/authorisations" component={Authorisations} />
    <Route path="/fileshare/:id" component={FileShare} exact />
    <Route path="/invoices/:id" component={InvoicesPayment} exact />
    <Route path="*" component={Drupal} />
  </Switch>
);
