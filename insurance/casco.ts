import { InsuranceBaseDto, InsuranceConsents } from 'app/dto/insurance';
import { PremiumPaymentPeriod } from 'helpers/date';
import { Option } from 'app/dto/field';

export interface InsuranceCascoDto extends InsuranceBaseDto {
  data: InsuranceCascoVehicleDto;
  client: InsuranceCascoClientDto;
  conclude: InsuranceCascoConcludeDto;
  sims?: InsuranceCascoSims;
  selectedPackage: InsurancePackage[];
  packageCode: string;
}

interface InsuranceCascoVehicleDto {
  vehicleNo: string;
  rangeOfUse: 'common_use' | 'taxi' | 'other';
  compensationVAT: boolean;
  // copy legalPerson, personalCode, companyCode from client dto
  // for first step request
  legalPerson: boolean;
  personalCode: string;
  companyCode: string;
  indemnityLimitForAccessories: number;
  frontAndCabinWindows: boolean;
  cascoDailyAllowance: boolean;
}

interface InsuranceCascoClientDto {
  legalPerson: boolean;
  firstName: string;
  lastName: string;
  personalCode: string;
  companyName: string;
  companyCode: string;
  phone: string;
  email: string;
  contactPerson: string;
  premiumPaymentPerYear: PremiumPaymentPeriod;
}

interface InsuranceCascoConcludeDto {
  validityArea: string;
  insuranceCoverage: string;
  salvaCarHelpService: string;
  insurancePeriod: string;
  conditions: boolean;
  photosCondition: boolean;
  price: InsuranceCascoPrice;
  consents: InsuranceConsents;
}

export interface InsuranceCascoPrice {
  currency: string;
  period: '1' | '5';
  deductible: '200' | '300' | '500';
  deductibleValues: string[];
  first: number;
  monthly: number;
  total: number;
  packageCode: string;
  yearly: number;
}

export interface InsuranceCascoSims {
  vehicle: string;
  prices: Array<InsuranceCascoPrice>;
  consents: InsuranceConsents;
}

export interface InsuranceCascoFinalPrice {
  price: InsuranceCascoPrice;
  consents: InsuranceConsents;
}

export interface InsurancePackage {
  packageCode:
    | 'mod_base'
    | 'mod_standard'
    | 'mod_premium'
    | 'ppd_base'
    | 'ppd_all';
  packageName: string;
  price?: number;
  groups: Array<InsurancePackageGroup>;
}

export interface InsuranceCoverage {
  booleanValue: boolean;
  insured: string;
  classifierValue: null;
  deductibleValue: string | null;
  description: string | null;
  dropdown: boolean;
  dropdownValues: Array<Option<string>>;
  fieldCode: string;
  fieldTypeCode: string | null;
  indemnityLimitValue: null;
  name: string;
}

export interface InsurancePackageGroup {
  name: string;
  order: number;
  coverages: Array<InsuranceCoverage>;
}
