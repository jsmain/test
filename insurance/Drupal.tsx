import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { SearchResults } from 'app/containers/Drupal/SearchResults';
import { useDispatch, useSelector } from 'app/store/utils';
import { getContent, setContent } from 'app/features/drupal/drupalActions';
import { contentSelector } from 'app/features/drupal/drupalSelectors';
import { DrupalContentPageType } from 'app/dto/drupal';
import { Grid, Hidden } from '@mui/material';
import { SideBar } from 'app/components/Drupal/SideBar';
import { MetaTags, MetaTagsRef } from 'app/common/MetaTags';
import { Loadable } from 'app/common/Loadable';
import { AxiosError } from 'axios';
import loadable from '@loadable/component';
import { SingleBanner } from 'app/components/Drupal/SingleBanner';
import { SingleBannerBlockDto } from 'app/dto/drupal/homepage';
import bannerImage from 'assets/images/banner.webp';

const Webform = loadable(() => import('app/containers/Drupal/Webform'));
const NewsDetails = loadable(() => import('app/containers/Drupal/NewsDetails'));
const NewsList = loadable(() => import('app/containers/Drupal/NewsList'));
const Contacts = loadable(() => import('app/containers/Drupal/Contacts'));
const InsuranceDetails = loadable(
  () => import('app/containers/Drupal/InsuranceDetails')
);
const DrupalArticle = loadable(() => import('app/containers/Drupal/Article'));
const DrupalHome = loadable(() => import('app/containers/Drupal/Home'));
const NotFound = loadable(() => import('app/containers/NotFound'));

const SEARCH_ROUTE = '/search';

const Drupal = () => {
  const dispatch = useDispatch();
  const { pathname, search } = useLocation();
  const [error, setError] = useState<null | number>();
  const isSearchRoute = pathname === SEARCH_ROUTE;
  const meta = useRef<MetaTagsRef>(null);
  const loading = useCallback(async () => {
    setError(null);
    if (isSearchRoute) return;
    try {
      const content = await dispatch(getContent({ pathname, search })).unwrap();
      meta.current?.setTags(content.meta);
    } catch (e) {
      const { response } = e as AxiosError;
      response?.status && response?.status !== 404 && setError(response.status);
    }
  }, [dispatch, isSearchRoute, pathname, search]);

  useEffect(
    () => () => {
      dispatch(setContent(undefined));
    },
    [dispatch]
  );

  const content = useSelector(contentSelector);
  const hasBanner = Boolean(content?.banner?.description);
  const hasMenu = Boolean(content?.menu?.length);
  const contentPage = useMemo(() => {
    switch (content?.type) {
      case DrupalContentPageType.home:
        return <DrupalHome data={content} />;
      case DrupalContentPageType.article:
        return <DrupalArticle data={content} />;
      case DrupalContentPageType.insurance:
        return <InsuranceDetails data={content} />;
      case DrupalContentPageType.contacts:
        return <Contacts data={content} />;
      case DrupalContentPageType.newsList:
        return <NewsList data={content} />;
      case DrupalContentPageType.news:
      case DrupalContentPageType.blog:
        return <NewsDetails data={content} />;
      case DrupalContentPageType.webform:
        return <Webform data={content} />;
      default:
        return <NotFound />;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content, pathname]);
  return (
    <>
      <MetaTags ref={meta} />
      <Loadable action={loading}>
        <div className="content">{renderContent()}</div>
      </Loadable>
    </>
  );

  function renderContent() {
    if (isSearchRoute) {
      return <SearchResults />;
    }

    return (
      <>
        <Grid container columnSpacing={4} rowSpacing={2}>
          {content && hasBanner && (
            <Grid item xs={12}>
              <SingleBanner
                type="singleBanner"
                data={
                  {
                    title: content.banner!.description,
                    bannerImage: bannerImage,
                  } as SingleBannerBlockDto['data']
                }
              />
            </Grid>
          )}
          {content && hasMenu && (
            <Hidden xlDown>
              <Grid item xl={3}>
                <SideBar links={content.menu!} />
              </Grid>
            </Hidden>
          )}
          <Grid item xs={12} xl={hasMenu ? 9 : 12}>
            {contentPage}
          </Grid>
        </Grid>
        {error && error}
      </>
    );
  }
};

export default Drupal;
