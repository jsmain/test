import { FC, useCallback, useContext, useMemo, useRef } from 'react';
import { Tooltip } from 'mui/components/Tooltip';
import { Typography } from 'mui/components/Typography';
import { GridList, GridListItem } from 'app/common/GridList';
import { WizardFormStep } from 'app/common/Form/FormWizard';
import { finalFields } from 'app/common/Form/FinalFields';
import { TransField } from 'i18n/trans/field';
import {
  InsuranceCascoDto,
  InsuranceCascoFinalPrice,
} from 'app/dto/insurance/casco';
import { compose, phone, required } from 'helpers/validate';
import { formatFloatPrice } from 'helpers/price';
import {
  CalculationLoader,
  CalculationLoaderRef,
} from 'app/components/Insurance/CalculationLoader';
import { FormProps, useForm } from 'react-final-form';
import { formSubmitThrowError } from 'helpers/formSubmit';
import { api } from 'app/features/api';
import { FormWatch } from 'app/common/Form/FormWatch';
import { formatDistanceToNowStrict, PremiumPaymentPeriod } from 'helpers/date';
import { addYears } from 'date-fns';
import { premiumPaymentPeriods } from 'helpers/date';
import {
  companyCodeMask,
  personalCodeMask,
  phoneMask,
} from 'fields/MaskedField';
import { TransValidate } from 'i18n/trans/validate';
import { CascoButtons } from 'app/components/Insurance/Casco/CascoButtons';
import { LabelField } from 'fields/LabelField';
import { RadioGroupField } from 'fields/RadioGroupField';
import { InsurancePackagesContext } from 'app/components/Insurance/Packages';

interface CascoClientProps {}

export const CascoClient: FC<CascoClientProps> = () => {
  const { packages, insertValues } = useContext(InsurancePackagesContext);
  const { getState } = useForm<InsuranceCascoDto>();
  const { values } = getState();

  const getSelectedPackageName = useCallback(() => {
    const packageItem = packages.find(
      (p) => p.packageCode === values.packageCode
    );
    return packageItem ? (
      packageItem.packageName
    ) : (
      <TransField i18nKey="insuranceCoverage.superCasco" />
    );
  }, [packages, values.packageCode]);

  const getSelectedPackage = useCallback(
    (selectedValues: Record<string, Record<string, string>>) => {
      return insertValues(selectedValues).find(
        (p) => p.packageCode === values.packageCode
      );
    },
    [insertValues, values.packageCode]
  );

  const fields = useMemo(
    () =>
      finalFields<InsuranceCascoDto>([
        {
          key: 'policyHolder.title',
          type: 'component',
          component: (
            <Typography fontWeight={700}>
              <TransField i18nKey="policyHolderInformation" />
              <Tooltip
                title={<TransField i18nKey="policyHolderInformationTip" />}
              />
            </Typography>
          ),
        },
        {
          i18n: 'firstName',
          name: 'client.firstName',
          type: 'text',
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: false },
        },
        {
          i18n: 'lastName',
          name: 'client.lastName',
          type: 'text',
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: false },
        },
        {
          i18n: 'personalCode',
          name: 'client.personalCode',
          type: 'tel',
          mask: personalCodeMask,
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: false },
        },
        {
          i18n: 'companyName',
          name: 'client.companyName',
          type: 'text',
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: true },
        },
        {
          i18n: 'companyCode',
          name: 'client.companyCode',
          type: 'tel',
          mask: companyCodeMask,
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: true },
        },
        {
          i18n: 'phone',
          name: 'client.phone',
          type: 'tel',
          mask: phoneMask,
          required: true,
          validate: compose(
            required(),
            phone(<TransValidate i18nKey="phoneNumber" />)
          ),
        },
        {
          i18n: 'email',
          name: 'client.email',
          type: 'email',
          required: true,
          validate: required(),
        },
        {
          i18n: 'contactPerson',
          name: 'client.contactPerson',
          type: 'text',
          required: true,
          validate: required(),
          condition: { when: 'client.legalPerson', is: true },
        },
        {
          key: 'client.premiumPaymentPerYear',
          type: 'component',
          render: () => {
            return (
              <>
                <GridListItem sx={{ mt: '56px !important' }} required>
                  <LabelField name="client.premiumPaymentPerYear" required>
                    <TransField i18nKey="premiumPaymentPerYear" />
                  </LabelField>
                  <RadioGroupField
                    defaultValue={1 as PremiumPaymentPeriod}
                    name={'client.premiumPaymentPerYear'}
                    options={premiumPaymentPeriods.map((value) => {
                      return {
                        value,
                        label: (
                          <TransField
                            i18nKey={`premiumPaymentPerYear.${value}`}
                          />
                        ),
                      };
                    })}
                    validate={required()}
                    row
                  />
                </GridListItem>
              </>
            );
          },
        },
        {
          key: 'policyHolder.divider',
          type: 'divider',
        },
        {
          i18n: 'vehicle',
          name: 'sims.vehicle',
          render: ({ input: { value } }) => (
            <Typography fontWeight={700}>{value}</Typography>
          ),
        },
        {
          name: 'conclude.price.period',
          i18n: 'insurancePeriod',
          render: ({ input: { value } }) => (
            <Typography fontWeight={700}>
              {formatDistanceToNowStrict(addYears(new Date(), +value))}
            </Typography>
          ),
        },
        {
          name: 'conclude.insuranceCoverage',
          i18n: 'insuranceCoverage',
          render: () => (
            <Typography fontWeight={700}>{getSelectedPackageName()}</Typography>
          ),
        },
        {
          name: 'conclude.price.first',
          i18n: 'firstPayment',
          render: ({ input: { value } }) => (
            <Typography fontWeight={700} color="primary">
              {formatFloatPrice(value)}
            </Typography>
          ),
          condition: [
            {
              when: 'conclude.price.first',
              is: Boolean,
            },
            {
              when: 'client.premiumPaymentPerYear',
              is: ['2', '4', '12'],
            },
          ],
        },
        {
          name: 'conclude.price.yearly',
          i18n: 'totalPaymentPerYear',
          render: ({ input: { value } }) => (
            <Typography fontWeight={700} color="primary">
              {formatFloatPrice(value)}
            </Typography>
          ),
        },
      ]),
    [getSelectedPackageName]
  );

  const calculationRef = useRef<CalculationLoaderRef>(null);
  const form = useForm();
  const handleSubmit: FormProps<InsuranceCascoDto>['onSubmit'] = useCallback(
    (values) =>
      formSubmitThrowError(async () => {
        calculationRef.current?.open();
        try {
          const { packages, ...valueList } = values;
          const {
            data: { price, consents },
          } = await api.post<InsuranceCascoFinalPrice>(
            '/insurance/casco/final-price',
            {
              ...valueList,
              packages: [getSelectedPackage(packages)],
            }
          );
          form.change('selectedPackage', [getSelectedPackage(packages)]);
          form.change('sims.consents', consents);
          form.change('conclude.price', price);
        } finally {
          calculationRef.current?.close();
        }
      }),
    [form, getSelectedPackage]
  );

  return (
    <WizardFormStep onSubmit={handleSubmit}>
      <CalculationLoader ref={calculationRef} />

      <FormWatch submitOnMount />

      <GridList maxWidth="none">{fields}</GridList>
      <CascoButtons />
    </WizardFormStep>
  );
};
