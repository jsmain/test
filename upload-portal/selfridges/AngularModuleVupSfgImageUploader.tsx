import React, { lazy, Suspense } from 'react';
import { Loader } from '@source/components/base/Loader';
import { CFC } from '@source/uikit';

const VupSfgImage = lazy(() =>
  import('@source/modules/vendor-upload-portal/selfridges/VupSfgImage').then((Import) => ({
    default: Import.VupSfgImage,
  })),
);

export const AngularModuleVupSfgImageUploader: CFC = () => {
  return (
    <Suspense fallback={<Loader />}>
      <VupSfgImage />
    </Suspense>
  );
};
