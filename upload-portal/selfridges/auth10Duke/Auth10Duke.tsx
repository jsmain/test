import React, { useContext, useState } from 'react';
import { Button, ChildrenProps, TextField } from '@source/uikit';
import { i18n } from '@source/i18n/i18n';
import { Controller, useForm } from 'react-hook-form';
import { useAxiosRequest } from '@source/hooks/base/useAxiosRequest';
import { LocalStorage } from '@source/utils/LocalStorage';
import { Auth10DukeContext } from '@source/contexts/Auth10DukeProvider';
import styles from './Auth10Duke.scss';
import { useYupValidationResolver } from '@source/hooks/yup/useYupValidationResolver';
import { SelfridgesService } from '@source/api/services/SelfridgesService';
import { sfgConstants } from '@source/modules/vendor-upload-portal/selfridges/sfgConstants';
import { i18nButtonName } from '@source/utils/i18nButtonName';
import { auth10DukeSchema } from '@source/modules/vendor-upload-portal/validation-schemas/auth10DukeSchema';
import { FlexColumn } from '@source/uikit/layout';

interface Auth10DukeFormProps {
  email: string;
  password: string;
}

export const Auth10Duke: React.FC<ChildrenProps> = ({ children }) => {
  const { auth, updateAuth } = useContext(Auth10DukeContext);
  const [authError, setAuthError] = useState('');

  const [authWith10Duke, , { inProgress }] = useAxiosRequest(SelfridgesService.login);
  const resolver = useYupValidationResolver(auth10DukeSchema);

  const { handleSubmit, formState, control, reset } = useForm<Auth10DukeFormProps>({
    resolver,
    mode: 'all',
    defaultValues: {
      email: '',
      password: '',
    },
  });

  const handleSubmitForm = async ({ email, password }: Auth10DukeFormProps) => {
    const [response, status] = await authWith10Duke({ login: email.trim(), password });
    if (status.error) {
      let errorMessage = status.error.response?.data?.message || '';
      if (status.code === 401) {
        errorMessage = i18n.t('vupSfg.auth.error.login401', {
          defaultValue: 'Your username or password was incorrect.',
        });
      }
      if (status.code === 403) {
        errorMessage = i18n.t('vupSfg.auth.error.login403', {
          defaultValue: 'You are not authorized to access this portal.',
        });
      }
      setAuthError(errorMessage);
    } else {
      LocalStorage.set(sfgConstants.sfgVupSession, response);
      updateAuth(response);
      setAuthError('');
      reset();
    }
  };

  if (auth) {
    return <>{children}</>;
  }

  return (
    <FlexColumn>
      <div className={styles.vupSfgAuthWrapper}>
        <h2>{i18n.t('vupSfg.auth.welcomeBack', { defaultValue: 'Welcome back!' })}</h2>
        <div className={styles.vupSfgAuth}>
          {authError && <span className={styles.vupSfgAuthFormError}>{authError}</span>}
          <form onSubmit={handleSubmit(handleSubmitForm)}>
            <Controller
              control={control}
              name="email"
              render={({ field: { onChange, value } }) => (
                <div className={styles.vupSfgAuthForm}>
                  <div className={styles.vupSfgAuthFormLabel}>
                    {i18n.t('vupSfg.auth.emailLabel', { defaultValue: 'Enter your email address' })}
                  </div>
                  <TextField
                    name="email"
                    placeholder={i18n.t('vupSfg.auth.emailPlaceholder', { defaultValue: 'Your email address' })}
                    value={value}
                    onChange={onChange}
                    isInvalid={formState.isSubmitted && !!formState.errors.email}
                  />
                  {formState.isSubmitted && formState.errors.email && (
                    <div className={styles.vupSfgAuthFormError}>{formState.errors.email.message}</div>
                  )}
                </div>
              )}
            />
            <Controller
              control={control}
              name="password"
              render={({ field: { onChange, value } }) => (
                <div className={styles.vupSfgAuthForm}>
                  <div className={styles.vupSfgAuthFormLabel}>
                    {i18n.t('vupSfg.auth.passwordLabel', { defaultValue: 'Enter your password' })}
                  </div>
                  <TextField
                    isPassword={true}
                    name="password"
                    placeholder={i18n.t('vupSfg.auth.passwordPlaceholder', { defaultValue: 'Your password' })}
                    value={value}
                    onChange={onChange}
                    isInvalid={formState.isSubmitted && !!formState.errors.password}
                  />
                  {formState.isSubmitted && formState.errors.password && (
                    <div className={styles.vupSfgAuthFormError}>{formState.errors.password.message}</div>
                  )}
                </div>
              )}
            />
            <div className={styles.vupSfgAuthBtnSubmit}>
              <Button isDisabled={inProgress} type="submit" fullWidth={true}>
                {i18nButtonName('signIn')}
              </Button>
            </div>
          </form>
        </div>
      </div>
    </FlexColumn>
  );
};
