export const vup-sfg-auth: string;
export const vupSfgAuth: string;
export const vup-sfg-auth__wrapper: string;
export const vupSfgAuthWrapper: string;
export const vup-sfg-auth__form: string;
export const vupSfgAuthForm: string;
export const vup-sfg-auth__form__label: string;
export const vupSfgAuthFormLabel: string;
export const vup-sfg-auth__form--error: string;
export const vupSfgAuthFormError: string;
export const vup-sfg-auth__btn-submit: string;
export const vupSfgAuthBtnSubmit: string;
