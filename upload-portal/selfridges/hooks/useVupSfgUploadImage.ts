import { useContext, useState } from 'react';
import path from 'path';
import { chain, fromPairs, size, some } from 'lodash';
import { FolderList } from '@source/components/drag-folder/drag-folder.component';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import {
  createFolder,
  populateFolderWithImages,
  updateFolderName,
} from '@source/modules/vendor-upload-portal/helpers/folder-creator';
import { imagesCreator } from '@source/modules/vendor-upload-portal/helpers/image-creator';
import { VupFolderState } from '@source/modules/vendor-upload-portal/types/VupFolderState';
import { vupValidateFolders } from '@source/modules/vendor-upload-portal/selfridges/helpers/vupSfgValidateFolder';
import { vupSfgFilterUploadingImages } from '@source/modules/vendor-upload-portal/selfridges/helpers/vupSfgFilterUploadingImages';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { Auth10DukeContext } from '@source/contexts/Auth10DukeProvider';
import { RequestState } from '@source/types/enums/RequestState';
import { uploadingQueue } from '@source/modules/vendor-upload-portal/helpers/uploading-queue';
import { SelfridgesService } from '@source/api/services/SelfridgesService';
import { iterateObject } from '@source/utils/iterateObject';
import { getUploadErrorMessage } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/helpers/upload';
import { VupImageState } from '@source/modules/vendor-upload-portal/types/VupImageState';

interface NewDirFirstFile extends File {
  webkitRelativePath?: string;
}

export interface SfgUploadImageState {
  folders: VupFolderState;
  updateFolderList: (folders: FolderList) => void;
  editFolderName: (newName: string, oldName: string) => void;
  deleteFolder: (name: string) => void;
  deleteImage: (imageName: string, folderName: string) => void;
  addImage: (fileList: FileList, folderName: string) => void;
  editImageName: (newImageName: string, imageName: string, folderName: string) => void;
  addFolder: (files: FileList) => void;
  upload: () => void;
  reset: () => void;
  uploadComplete: boolean;
}

const maxFolders = 50;

export const useVupSfgUploadImage = (): SfgUploadImageState => {
  const { auth, signOut } = useContext(Auth10DukeContext);

  const [folders, setFolders] = useState<VupFolderState>({});
  const [uploadComplete, setUploadComplete] = useState(false);

  const handleSetFolders = (folders: VupFolderState) => {
    const validatedFolders = vupValidateFolders(folders);
    setFolders(validatedFolders);
  };

  const updateFolderList = async (newFolders: FolderList) => {
    const currentFolders = { ...folders };
    const stateFolders = {};
    for (const key in newFolders) {
      if (newFolders.hasOwnProperty(key) && size(folders) < maxFolders) {
        const folderFiles = newFolders[key];
        const checkedImages = vupSfgFilterUploadingImages(folderFiles);
        if (checkedImages.length) {
          stateFolders[key] = await processFolder(key, checkedImages);
          handleSetFolders({ ...currentFolders, ...stateFolders });
        }
      }
    }
  };

  const processFolder = async (folderName, folderImages): Promise<VupFolderDetails> => {
    const folder = createFolder(folderName);
    const images = await imagesCreator(folderImages, folderName);
    return populateFolderWithImages(folder, images);
  };

  const editFolderName = (newName, oldName) => {
    const currentFolders = { ...folders };
    const folderToRename = { ...currentFolders[oldName] };
    delete currentFolders[oldName];
    const imagesEntries = Object.entries(folderToRename.images).map(([, image]: [string, VupImageDetails]) => {
      return [image.name, { ...image, path: path.resolve(newName, image.name) }];
    });
    folderToRename.images = fromPairs(imagesEntries);
    const newFolder = updateFolderName(folderToRename, newName);
    handleSetFolders({ ...currentFolders, [newName]: { ...newFolder } });
  };

  const deleteFolder = (name) => {
    const currentFolders = { ...folders };
    delete currentFolders[name];
    setFolders(currentFolders);
  };

  const deleteImage = (imageName, folderName) => {
    const folder = { ...folders[folderName] };
    delete folder.images[imageName];
    if (!!size(folder.images)) {
      handleSetFolders({ ...folders, [folderName]: folder });
    } else {
      deleteFolder(folderName);
    }
  };

  const addImage = async (files, folderName) => {
    const folder = { ...folders[folderName] };
    const newImages = await imagesCreator(files, folderName);
    const folderImages = { ...folder.images, ...newImages };
    const updatedFolder = populateFolderWithImages(folder, folderImages);
    handleSetFolders({ ...folders, [folderName]: updatedFolder });
  };

  const editImageName = (newImageName, imageName, folderName) => {
    const folder = { ...folders[folderName] };
    const image = folder.images[imageName];
    image.name = newImageName;
    folder.images[newImageName] = image;
    delete folder.images[imageName];
    handleSetFolders({ ...folders, [folderName]: folder });
  };

  const addFolder = (files) => {
    if (size(folders) < maxFolders) {
      const firstFile: NewDirFirstFile = files[0];
      const dirName = firstFile.webkitRelativePath.split('/')[0];
      void updateFolderList({ [dirName]: [...files] });
    }
  };

  const checkCompleteUpload = () => {
    setFolders((folders) => {
      const isAllFilesUploaded = chain(folders)
        .every((folder) =>
          chain(folder.images)
            .every((image) => image.uploadingRequestState === RequestState.success)
            .value(),
        )
        .value();
      if (size(folders) && isAllFilesUploaded) {
        setUploadComplete(true);
      }
      return folders;
    });
  };

  const imageUploader = (folderName: string, imagesList: VupImageDetails[]) => {
    const requestPromises = imagesList.map((image) => async () => {
      try {
        await SelfridgesService.uploadImage({
          file: image.file,
          path: image.path,
          sfgVupSession: auth,
        });
        setUploadedImageStatus(folderName, { ...image }, 200);
      } catch (e) {
        if (e.response.status === 403) {
          signOut();
          reset();
        } else {
          setUploadedImageStatus(folderName, { ...image }, e.response?.status || 500);
        }
      }
    });

    if (auth) {
      setRequestUploadImageStatus(folderName, imagesList);
      uploadingQueue(requestPromises, folderName, checkCompleteUpload);
    }
  };

  const setRequestUploadImageStatus = (folderName: string, images: VupImageDetails[]) => {
    setFolders((folders) => {
      const folder: VupFolderDetails = folders[folderName];
      const updatedImages: VupImageState = {};
      Object.entries(folder.images).map(([imageName, image]: [string, VupImageDetails]) => {
        const updatedImage = { ...image };
        if (images.find((image) => image.name === updatedImage.name)) {
          updatedImage.uploadingRequestState =
            image.uploadingRequestState === RequestState.success ? image.uploadingRequestState : RequestState.request;
        }
        updatedImages[imageName] = updatedImage;
      });
      const updatedFolder = populateFolderWithImages(folder, updatedImages);
      updatedFolder.uploading = true;
      return { ...folders, [folderName]: updatedFolder };
    });
  };

  const setFolderUploadingStatus = () => {
    setFolders((folders) => {
      return iterateObject<VupFolderState>(folders, (key, folder) => {
        folder.uploading = true;
        return {
          key,
          value: { ...folder },
        };
      });
    });
  };

  const setUploadedImageStatus = (folderName: string, image: VupImageDetails, uploadingStatusCode) => {
    setFolders((folders) => {
      const folder = { ...folders[folderName] };
      const updatedImage = {
        ...image,
        uploadingRequestState: uploadingStatusCode === 200 ? RequestState.success : RequestState.failure,
        uploadingErrorMessage: getUploadErrorMessage(uploadingStatusCode),
      };
      const folderImages = { ...folder.images, [image.name]: updatedImage };
      const updatedFolder = populateFolderWithImages(folder, folderImages);
      if (updatedImage.uploadingRequestState === RequestState.success) {
        updatedFolder.uploadedImagesCount++;
      }
      updatedFolder.uploadingImagesProgress = Math.floor(
        (updatedFolder.uploadedImagesCount * 100) / size(folderImages),
      );
      updatedFolder.uploading = some(folderImages, (image) => image.uploadingRequestState === RequestState.request);
      return { ...folders, [folderName]: updatedFolder };
    });
  };

  const upload = () => {
    setFolderUploadingStatus();
    const folderKeys = Object.keys(folders).sort((a, b) => a.localeCompare(b));
    for (let i = 0; i < folderKeys.length; i++) {
      const folderKey = folderKeys[i];
      const { name, images } = folders[folderKey];
      imageUploader(
        name,
        Object.entries(images).map(([, image]) => image),
      );
    }
  };

  const reset = () => {
    setFolders({});
    setUploadComplete(false);
  };

  return {
    folders,
    uploadComplete,
    updateFolderList,
    editFolderName,
    deleteFolder,
    deleteImage,
    addImage,
    editImageName,
    addFolder,
    upload,
    reset,
  };
};
