import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { i18n } from '@source/i18n/i18n';

const minWidth = 2000;

export const vupSfgValidateFolderImage = (image: VupImageDetails): VupImageDetails => {
  image.errors = [];
  if (image.width < minWidth) {
    image.errors.push({
      type: 'width',
      message: i18n.t('vupSfg.error.imageWidth', { defaultValue: 'Image should be at least 2000px width.' }),
    });
  }
  return image;
};
