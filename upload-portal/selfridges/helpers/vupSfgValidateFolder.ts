import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import { iterateObject } from '@source/utils/iterateObject';
import { VupFolderState } from '@source/modules/vendor-upload-portal/types/VupFolderState';
import { i18n } from '@source/i18n/i18n';
import { vupSfgValidateFolderImage } from '@source/modules/vendor-upload-portal/selfridges/helpers/vupSfgValidateFolderImage';
import { VupImageState } from '@source/modules/vendor-upload-portal/types/VupImageState';

export const vupValidateFolders = (folders: VupFolderState): VupFolderState => {
  return iterateObject<VupFolderState>(folders, (folderKey, folder) => {
    return { key: folderKey, value: vupSfgValidateFolder(folder) };
  });
};

export const vupSfgValidateFolder = (folder: VupFolderDetails): VupFolderDetails => {
  const foldersNameLength = 9;
  folder.errors = [];
  folder.isValid = true;
  if (folder.name.length !== foldersNameLength && !/^\d+$/.test(folder.name)) {
    folder.errors.push(
      i18n.t('vupSfg.error.folderName', { defaultValue: 'Folder should be named with a 9-digit SKU.' }),
    );
    folder.isValid = false;
  }
  folder.images = iterateObject<VupImageState>(folder.images, (folderKey, image) => {
    const validatedImage = vupSfgValidateFolderImage(image);
    if (validatedImage.errors.length) {
      folder.isValid = false;
    }
    return { key: folderKey, value: validatedImage };
  });
  return folder;
};
