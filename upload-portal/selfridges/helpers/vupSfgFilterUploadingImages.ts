export const vupSfgFilterUploadingImages = (images: File[]): File[] => {
  return images.filter((image) => {
    if (image.type) {
      return image.type === 'image/jpeg';
    }
    return /\.(jpe?g)$/i.test(image.name);
  });
};
