import React from 'react';
import { CFC, CheckCircleSvg } from '@source/uikit';
import style from './VupSfgSuccessUpload.scss';
import { i18n } from '@source/i18n/i18n';

interface VupSfgSuccessUploadProps {
  onReset: () => void;
}

export const VupSfgSuccessUpload: CFC<VupSfgSuccessUploadProps> = ({ onReset }) => {
  return (
    <div className={style.vupSfgSuccessUpload}>
      <div className={style.vupSfgSuccessUploadContent}>
        <div className={style.vupSfgSuccessUploadIcon}>
          <CheckCircleSvg />
        </div>
        <h2>
          {i18n.t('vupSfg.uploadComplete.title', { defaultValue: 'All folders have been successfully uploaded!' })}
        </h2>
        <div className={style.vupSfgSuccessUploadContinue}>
          {i18n.t('vupSfg.uploadComplete.uploadMore', { defaultValue: 'To upload more folders' })},
          <div className={style.vupSfgSuccessUploadContinueNew} onClick={onReset}>
            &nbsp; {i18n.t('vupSfg.uploadComplete.startNewSession', { defaultValue: 'start a new session' })}.
          </div>
        </div>
      </div>
    </div>
  );
};
