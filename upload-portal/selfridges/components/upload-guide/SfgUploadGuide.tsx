import styles from './SfgUploaderGuide.scss';
import { i18n } from '@source/i18n/i18n';
import { CFC } from '@source/uikit';

export const SfgUploaderGuide: CFC = () => {
  return (
    <div className={styles.sfgUploaderGuide}>
      <div className={styles.sfgUploaderGuideWrapper}>
        <h3>Guidelines</h3>
        <ul>
          <li>{i18n.t('vupSfg.guide.rule1', { defaultValue: 'One folder per SKU.' })}</li>
          <li>{i18n.t('vupSfg.guide.rule2', { defaultValue: 'Folder must use 9 digit numerical SKU.' })}</li>
          <li>{i18n.t('vupSfg.guide.rule3', { defaultValue: 'Images must be in JPEG format.' })}</li>
          <li>{i18n.t('vupSfg.guide.rule4', { defaultValue: 'Images should be at least 2000px width.' })}</li>
          <li>{i18n.t('vupSfg.guide.rule5', { defaultValue: 'Upload a maximum of 50 folders at a time.' })}</li>
        </ul>
      </div>
    </div>
  );
};
