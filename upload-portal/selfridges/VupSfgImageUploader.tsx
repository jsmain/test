import React, { useContext, useMemo } from 'react';
import { size } from 'lodash';
import { Button, CFC, PlusSvg, UploadDirButton } from '@source/uikit';
import styles from './VupSfgImageUploader.scss';
import { i18n } from '@source/i18n/i18n';
import {
  SfgUploadImageState,
  useVupSfgUploadImage,
} from '@source/modules/vendor-upload-portal/selfridges/hooks/useVupSfgUploadImage';
import { DragFolder } from '@source/components/drag-folder/drag-folder.component';
import { SfgUploaderGuide } from '@source/modules/vendor-upload-portal/selfridges/components/upload-guide/SfgUploadGuide';
import { VupTable, VupTableColumnTitle } from '@source/modules/vendor-upload-portal/components/table/VupTable';
import { FolderUploadSvg } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/icons/folder-upload-svg.component';
import { Auth10Duke } from '@source/modules/vendor-upload-portal/selfridges/auth10Duke/Auth10Duke';
import { Auth10DukeContext } from '@source/contexts/Auth10DukeProvider';
import { VupSfgSuccessUpload } from '@source/modules/vendor-upload-portal/selfridges/components/success-upload/VupSfgSuccessUpload';
import { i18nButtonName } from '@source/utils/i18nButtonName';

const smLink = 'https://www.spinme.co.uk/';
const spinMeLogo = 'https://sirv.sirv.com/website/SpinMeLogo.svg';

export const VupSfgImageUploader: CFC = () => {
  const { auth, signOut } = useContext(Auth10DukeContext);
  const state: SfgUploadImageState = useVupSfgUploadImage();
  const isValidFolders = useMemo(() => !Object.entries(state.folders).some(([, folder]) => !folder.isValid), [
    state.folders,
  ]);

  const isUploading = useMemo(() => Object.entries(state.folders).some(([, folder]) => folder.uploading), [
    state.folders,
  ]);

  const handleSignOut = () => {
    signOut();
    state.reset();
  };

  const renderFooter = () => {
    if (isUploading) {
      return (
        <div className={styles.vupSfgFooterUpload}>
          {i18n.t('vupSfg.footer.uploading', { defaultValue: 'Uploading images' })}...
        </div>
      );
    }
    return (
      <>
        <div className={styles.vupSfgFooterAdd}>
          <UploadDirButton onChangeDir={state.addFolder}>
            <div className={styles.vupSfgFooterAddButton}>
              <PlusSvg />
              <span>{i18n.t('vupSfg.footer.addFolder', { defaultValue: 'Add Folder' })}</span>
            </div>
          </UploadDirButton>
        </div>
        <Button variant="outline-secondary" onClick={state.reset}>
          {i18nButtonName('cancel')}
        </Button>
        <Button variant="primary" onClick={() => state.upload()} isDisabled={!isValidFolders}>
          {i18nButtonName('upload')}
        </Button>
      </>
    );
  };

  const renderUploadContent = () => {
    if (size(state.folders)) {
      return (
        <VupTable
          folders={state.folders}
          editFolderName={state.editFolderName}
          deleteFolder={state.deleteFolder}
          deleteImage={state.deleteImage}
          addImage={state.addImage}
          editImageName={state.editImageName}
          renderFooter={renderFooter()}
          columnTitles={{
            [VupTableColumnTitle.Content]: i18n.t('vupSfg.table.content', { defaultValue: 'Folder' }),
            [VupTableColumnTitle.Count]: i18n.t('vupSfg.table.count', { defaultValue: 'Images' }),
            [VupTableColumnTitle.Status]: i18n.t('vupSfg.table.status', { defaultValue: 'Status' }),
          }}
        />
      );
    }
    return (
      <div className={styles.vupSfgContentUploadBlockWrapper}>
        <h2>{i18n.t('vupSfg.upload.title', { defaultValue: 'Welcome to Supplied asset portal' })}</h2>
        <div className={styles.vupSfgContentUploadIcon}>
          <FolderUploadSvg />
        </div>
        <p>{i18n.t('spinUploader.dragFolders', { defaultValue: 'Drag and drop folders here' })}</p>
      </div>
    );
  };

  const renderContent = () => {
    if (state.uploadComplete) {
      return <VupSfgSuccessUpload onReset={state.reset} />;
    }
    return (
      <DragFolder onDropFolder={state.updateFolderList}>
        <div className={styles.vupSfgContentUpload}>
          <div className={styles.vupSfgContentUploadBlock}>{renderUploadContent()}</div>
          <SfgUploaderGuide />
        </div>
      </DragFolder>
    );
  };

  return (
    <div className={styles.vupSfg}>
      <div className={styles.vupSfgHeader}>
        <a className={styles.vupSfgHeaderLogo} href={smLink}>
          <img src={spinMeLogo} alt="logo" />
        </a>
        <h1>{i18n.t('vupSfg.title', { defaultValue: 'Supplied Asset Portal' })}</h1>
        {auth && (
          <Button variant="link" onClick={handleSignOut}>
            {i18nButtonName('signOut')}
          </Button>
        )}
      </div>
      <Auth10Duke>
        <div className={styles.vupSfgContent}>{renderContent()}</div>
      </Auth10Duke>
    </div>
  );
};
