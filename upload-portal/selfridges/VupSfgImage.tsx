import React from 'react';
import { CFC } from '@source/uikit';
import { Auth10DukeProvider } from '@source/contexts/Auth10DukeProvider';
import { VupSfgImageUploader } from '@source/modules/vendor-upload-portal/selfridges/VupSfgImageUploader';
export const VupSfgImage: CFC = () => {
  return (
    <Auth10DukeProvider>
      <VupSfgImageUploader />
    </Auth10DukeProvider>
  );
};
