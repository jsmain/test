import * as yup from 'yup';
import { i18n } from '@source/i18n/i18n';
import { EMAIL_REGEXP } from '@source/constants/email-regexp';

export const auth10DukeSchema = yup.object().shape({
  email: yup
    .string()
    .label(i18n.t('settings.personal.personalInformationSettings.email').toLocaleLowerCase())
    .required()
    .matches(
      EMAIL_REGEXP,
      i18n.t('vupSfg.auth.error.emailNotValid', { defaultValue: 'Please enter a valid email address' }),
    ),
  password: yup
    .string()
    .label(i18n.t('settings.personal.securityPassword').toLocaleLowerCase())
    .required(i18n.t('vupSfg.auth.error.emptyPassword', { defaultValue: 'Please enter a password' })),
});
