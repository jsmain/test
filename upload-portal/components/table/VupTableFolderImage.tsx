import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import cn from 'classnames';
import { usePopperTooltip } from 'react-popper-tooltip';
import { AlertSvg, Button, CFC, CheckCircleSvg, DeleteSvg, EditSvg, TextField, Tooltip } from '@source/uikit';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { ClickOutside } from '@source/components/base/ClickOutside';
import { i18n } from '@source/i18n/i18n';
import { RequestState } from '@source/types/enums/RequestState';
import style from './VupTableFolderImage.scss';
import tooltipStyles from 'react-popper-tooltip/dist/styles.css';
import { KeyboardEvent } from '@source/components/base/KeyboardEvent';
import { handleKeyboardEvent } from '@source/utils/handleKeyboardEvent';

interface VupTableFolderImageProps {
  image: VupImageDetails;
  onRename: (imageName: string, newImageName: string) => void;
  onDelete: (imageName: string) => void;
}

export const VupTableFolderImage: CFC<VupTableFolderImageProps> = (props) => {
  const [name, setName] = useState('');
  const [edit, setEdit] = useState(false);
  const [hover, setHover] = useState(false);

  useEffect(() => {
    setName(props.image.name);
  }, [props.image.name]);

  useEffect(() => {
    if (!edit && name && props.image.name !== name) {
      props.onRename(props.image.name, name);
    }
  }, [edit]);

  const { getArrowProps, getTooltipProps, setTooltipRef, setTriggerRef, visible } = usePopperTooltip(
    {
      trigger: 'hover',
      placement: 'top',
    },
    {
      strategy: 'fixed',
      modifiers: [
        {
          name: 'preventOverflow',
          options: {
            altAxis: true,
            altBoundary: true,
            padding: 0,
          },
        },
      ],
    },
  );

  const handleEdit = () => {
    setEdit(true);
  };

  const handleDelete = (e: React.MouseEvent) => {
    e.stopPropagation();
    props.onDelete(props.image.name);
  };

  const handleKeyDown = (e) => {
    handleKeyboardEvent(e, 'Enter', () => setEdit(false));
  };

  const renderImageName = () => {
    if (edit) {
      return (
        <KeyboardEvent onKeyDown={handleKeyDown}>
          <ClickOutside onClick={() => setEdit(false)}>
            <div className={style.vupTableFolderImageContentInfoNameEdit}>
              <TextField value={name} onChange={setName} autoFocus />
            </div>
          </ClickOutside>
        </KeyboardEvent>
      );
    }
    return props.image.name;
  };

  const renderImageSize = () => {
    return `${props.image.width} x ${props.image.height}`;
  };

  const renderImageError = () => {
    if (props.image.uploadingRequestState === RequestState.success) {
      return (
        <div className={style.vupTableFolderImageUploadSuccess}>
          {i18n.t('spinUploader.validation.uploadSuccess', { defaultValue: 'Successfully uploaded' })}
        </div>
      );
    }
    if (!props.image.errors.length && props.image.uploadingRequestState === RequestState.none) {
      return (
        <div className={style.vupTableFolderImageValidationSuccessIcon}>
          <CheckCircleSvg />
        </div>
      );
    }
    if (props.image.uploadingRequestState === RequestState.failure && !props.image.errors.length) {
      return <span>{i18n.t('spinUploader.validation.uploadFailed', { defaultValue: 'Upload failed' })}</span>;
    }
    if (props.image.errors.length) {
      return (
        <>
          <span ref={setTriggerRef}>
            {props.image.errors.length} {i18n.t('spinUploader.validation.errors', { defaultValue: 'errors' })}
          </span>
          {visible &&
            ReactDOM.createPortal(
              <div ref={setTooltipRef} {...getTooltipProps({ className: tooltipStyles.tooltipContainer })}>
                <div {...getArrowProps({ className: tooltipStyles.tooltipArrow })} />
                <div className={style.vupTableFolderImageErrorPopover}>
                  {props.image.errors.map((error, index) => (
                    <div key={index} className={style.vupTableFolderImageErrorPopoverBlock}>
                      <AlertSvg />
                      <p>{error.message}</p>
                    </div>
                  ))}
                </div>
              </div>,
              document.body,
            )}
        </>
      );
    }
    return null;
  };

  const handleHover = () => {
    if (props.image.uploadingRequestState !== RequestState.request) {
      setHover(true);
    }
  };

  return (
    <div className={style.vupTableFolderImage}>
      <div className={style.vupTableFolderImageContent} onMouseOver={handleHover} onMouseOut={() => setHover(false)}>
        <div
          className={cn(style.vupTableFolderImageContentActions, {
            [style.vupTableFolderImageContentEditVisible]: hover,
          })}
        >
          <Tooltip text="Rename">
            {({ ref }) => (
              <Button ref={ref} variant="icon" onClick={handleEdit}>
                <EditSvg />
              </Button>
            )}
          </Tooltip>
          <Tooltip text="Delete">
            {({ ref }) => (
              <Button ref={ref} variant="icon" onClick={handleDelete}>
                <DeleteSvg />
              </Button>
            )}
          </Tooltip>
        </div>
        <div className={style.vupTableFolderImageContentImg}>
          <img src={props.image.previewURL} alt={props.image.name} />
        </div>
        <div className={style.vupTableFolderImageContentInfo}>
          <div className={style.vupTableFolderImageContentInfoName}>{renderImageName()}</div>
          <div className={style.vupTableFolderImageContentInfoSize}>{renderImageSize()}</div>
        </div>
        <div className={style.vupTableFolderImageContentStatus}>{renderImageError()}</div>
      </div>
    </div>
  );
};
