import { usePopperTooltip } from 'react-popper-tooltip';
import { i18n } from '@source/i18n/i18n';
import style from './VupFolderErrorDetailsPopover.scss';
import { AlertSvg, CFC } from '@source/uikit';
import tooltipStyles from 'react-popper-tooltip/dist/styles.css';

interface ThdFolderErrorDetailsPopoverProps {
  errorList: string[];
}

export const VupFolderErrorDetailsPopover: CFC<ThdFolderErrorDetailsPopoverProps> = (props) => {
  const { getArrowProps, getTooltipProps, setTooltipRef, setTriggerRef, visible } = usePopperTooltip({
    trigger: 'click',
  });

  const renderContent = () => {
    return props.errorList.map((error, index) => (
      <div key={index} className={style.vupFolderErrorDetailsBodyBlock}>
        <div className={style.vupFolderErrorDetailsBodyBlockIcon}>
          <AlertSvg />
        </div>
        <div className={style.vupFolderErrorDetailsBodyMessage}>
          <span>{error}</span>
        </div>
      </div>
    ));
  };

  return (
    <>
      <div className={style.vupFolderErrorDetailsButton} ref={setTriggerRef} onClick={(e) => e.stopPropagation()}>
        {visible
          ? i18n.t('spinUploader.validation.hideDetails', { defaultValue: 'Hide details' })
          : i18n.t('spinUploader.validation.showDetails', { defaultValue: 'Show details' })}
      </div>
      {visible && (
        <div ref={setTooltipRef} {...getTooltipProps({ className: tooltipStyles.tooltipContainer })}>
          <div {...getArrowProps({ className: tooltipStyles.tooltipArrow })} />
          <div className={style.vupFolderErrorDetailsBody}>{renderContent()}</div>
        </div>
      )}
    </>
  );
};
