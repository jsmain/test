import React, { useEffect, useMemo, useState } from 'react';
import classNames from 'classnames';
import { chain } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { Button, CFC, DeleteSvg, EditSvg, PlusSvg, TextField, Tooltip, UploadFileButton } from '@source/uikit';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import { VupFolderErrorDetailsPopover } from '@source/modules/vendor-upload-portal/components/table/folder-error-details/VupFolderErrorDetailsPopover';
import { VupTableFolderImage } from '@source/modules/vendor-upload-portal/components/table/VupTableFolderImage';
import { i18n } from '@source/i18n/i18n';
import style from './VupTableFolder.scss';
import imageStyle from './VupTableFolderImage.scss';
import { RequestState } from '@source/types/enums/RequestState';
import { ProgressBar } from '@source/modules/vendor-upload-portal/components/progress-bar/progress-bar.component';
import { ClickOutside } from '@source/components/base/ClickOutside';
import { KeyboardEvent } from '@source/components/base/KeyboardEvent';
import { handleKeyboardEvent } from '@source/utils/handleKeyboardEvent';

interface VupTableFolderProps {
  folder: VupFolderDetails;
  onRenameFolder: (currentValue: string, newValue: string) => void;
  onDeleteFolder: (name: string) => void;
  onDeleteImage: (imageName: string, folderName: string) => void;
  onAddImage: (fileList: FileList, folderName: string) => void;
  onRenameImage: (currentValue: string, newValue: string, folderName: string) => void;
}

export const VupTableFolder: CFC<VupTableFolderProps> = ({
  folder,
  onRenameFolder,
  onDeleteFolder,
  onDeleteImage,
  onAddImage,
  onRenameImage,
}) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const [edit, setEdit] = useState(false);
  const [folderName, setFolderName] = useState('');

  useEffect(() => {
    setFolderName(folder.name);
  }, [folder.name]);

  useEffect(() => {
    if (!edit && folderName && folder.name !== folderName) {
      onRenameFolder(folderName, folder.name);
    }
  }, [edit]);

  const renderThumbnail = useMemo(
    () => <div className={style.vupTableFolderImg} style={{ backgroundImage: `url("${folder.previewURL}")` }} />,
    [folder.previewURL],
  );

  const handleSetExpanded = (e: React.MouseEvent) => {
    e.stopPropagation();
    setIsExpanded(!isExpanded);
  };

  const handleOnKeyDown = (e) => {
    handleKeyboardEvent(e, 'Enter', () => setEdit(false));
  };

  const matchImagesRequestState = (state: RequestState) => {
    return chain(folder.images)
      .every((image) => image.uploadingRequestState !== state)
      .value();
  };

  const renderImageName = () => {
    if (edit) {
      return (
        <KeyboardEvent onKeyDown={handleOnKeyDown}>
          <ClickOutside onClick={() => setEdit(false)}>
            <TextField value={folderName} onChange={setFolderName} autoFocus />
          </ClickOutside>
        </KeyboardEvent>
      );
    }
    return folder.name;
  };

  const renderValidStatus = () => {
    if (matchImagesRequestState(RequestState.success) && !navigator.onLine) {
      return (
        <div className={style.vupTableFolderValidationError}>
          {i18n.t('spinUploader.validation.noInternetConnection')}
        </div>
      );
    }

    if (folder.uploading) {
      return <ProgressBar value={folder.uploadingImagesProgress} />;
    }

    if (!folder.uploading && (matchImagesRequestState(RequestState.none) || !navigator.onLine)) {
      if (
        chain(folder.images)
          .some((image) => image.uploadingRequestState !== RequestState.success)
          .value()
      ) {
        return (
          <div className={style.vupTableFolderValidationError}>
            {i18n.t('spinUploader.validation.someImageNotUploaded')}
          </div>
        );
      }

      return (
        <div className={style.vupTableFolderValidationSuccess}>{i18n.t('spinUploader.validation.uploadSuccess')}</div>
      );
    }

    if (folder.isValid) {
      return <div className={style.vupTableFolderValidationSuccess}>{i18n.t('spinUploader.validation.success')}</div>;
    }

    return (
      <div className={style.vupTableFolderValidationError}>
        <div className={style.vupTableFolderValidationErrorSummary}>{i18n.t('spinUploader.validation.failed')}</div>
        <div className={style.vupTableFolderValidationErrorSeparator}>|</div>
        <VupFolderErrorDetailsPopover errorList={folder.errors} />
      </div>
    );
  };

  const uploadImageBlock = (
    <div className={imageStyle.vupTableFolderImage} key="upload-image-block">
      <div className={imageStyle.vupTableFolderImageUploadImage}>
        <UploadFileButton
          acceptedFormats={['jpeg']}
          multiple={true}
          onChangeFiles={(files) => onAddImage(files, folder.name)}
        >
          <div className={imageStyle.vupTableFolderImageUploadImageContent}>
            <PlusSvg />
            <span>{i18n.t('spinUploader.addImages')}</span>
          </div>
        </UploadFileButton>
      </div>
    </div>
  );

  const handleEdit = (e: React.MouseEvent) => {
    e.stopPropagation();
    setEdit(!edit);
  };

  const handleDelete = (e: React.MouseEvent) => {
    e.stopPropagation();
    onDeleteFolder(folder.name);
  };

  const handleDeleteImage = (imageName: string) => {
    onDeleteImage(imageName, folder.name);
  };

  const handleRenameImage = (imageName: string, newImageName: string) => {
    onRenameImage(newImageName, imageName, folder.name);
  };

  const renderImageList = () => {
    if (isExpanded) {
      const images = chain(folder.images)
        .sortBy((image) => image.name)
        .map((image, index) => {
          return (
            <VupTableFolderImage key={index} image={image} onDelete={handleDeleteImage} onRename={handleRenameImage} />
          );
        })
        .value();
      images.unshift(uploadImageBlock);

      return <div className={style.vupTableFolderImageList}>{images}</div>;
    }
    return null;
  };

  const renderImageCount = () => {
    const totalImages = Object.keys(folder.images).length;
    if (folder.uploading || matchImagesRequestState(RequestState.none)) {
      return `${folder.uploadedImagesCount} ${i18n.t('common.of')} ${totalImages}`;
    }

    return totalImages;
  };

  const renderActionsSection = () => {
    if (folder.uploading || matchImagesRequestState(RequestState.none)) {
      return null;
    }

    return (
      <>
        <Tooltip text={i18n.t('spinUploader.rename')}>
          {({ ref }) => (
            <Button ref={ref} variant="icon" onClick={handleEdit}>
              <EditSvg />
            </Button>
          )}
        </Tooltip>
        <Tooltip text={i18n.t('spinUploader.delete')}>
          {({ ref }) => (
            <Button ref={ref} variant="icon" onClick={handleDelete}>
              <DeleteSvg />
            </Button>
          )}
        </Tooltip>
      </>
    );
  };

  return (
    <div className={classNames(style.vupTableFolderItem, { [style.vupTableFolderItemExpanded]: isExpanded })}>
      <div className={style.vupTableFolderDetails} onClick={handleSetExpanded}>
        <div className={style.vupTableFolderItemCol1}>
          <div className={style.vupTableFolderItemExpandIcon}>
            <Button variant="icon" onClick={handleSetExpanded}>
              <FontAwesomeIcon icon={faCaretRight} />
            </Button>
          </div>
          <div className={style.vupTableFolderThumbnail}>{renderThumbnail}</div>
          <div className={style.vupTableFolderName}>{renderImageName()}</div>
        </div>
        <div className={style.vupTableFolderCount}>{renderImageCount()}</div>
        <div className={style.vupTableFolderStatus}>{renderValidStatus()}</div>
        <div className={style.vupTableFolderActions}>{renderActionsSection()}</div>
      </div>
      {renderImageList()}
    </div>
  );
};
