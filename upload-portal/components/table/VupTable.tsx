import React from 'react';
import { size } from 'lodash';
import { VupTableFolder } from '@source/modules/vendor-upload-portal/components/table/VupTableFolder';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import { i18n } from '@source/i18n/i18n';
import { CFC } from '@source/uikit';
import style from './VupTable.scss';
import { VupFolderState } from '@source/modules/vendor-upload-portal/types/VupFolderState';

export enum VupTableColumnTitle {
  Content = 'content',
  Count = 'count',
  Status = 'status',
}

interface VupTableProps {
  folders: VupFolderState;
  editFolderName: (newName: string, oldName: string) => void;
  deleteFolder: (name: string) => void;
  deleteImage: (imageName: string, folderName: string) => void;
  addImage: (fileList: FileList, folderName: string) => void;
  editImageName: (newImageName: string, imageName: string, folderName: string) => void;
  renderFooter: React.ReactNode;
  columnTitles?: { [column in VupTableColumnTitle]: string };
}

const defaultTableColumnTitles = {
  [VupTableColumnTitle.Content]: i18n.t('spinUploader.omsid', { defaultValue: 'OMSID' }),
  [VupTableColumnTitle.Count]: i18n.t('spinUploader.images', { defaultValue: 'Images' }),
  [VupTableColumnTitle.Status]: i18n.t('spinUploader.status', { defaultValue: 'Status' }),
};

export const VupTable: CFC<VupTableProps> = ({
  folders,
  editFolderName,
  deleteFolder,
  deleteImage,
  addImage,
  editImageName,
  renderFooter,
  columnTitles,
}) => {
  const renderFolder = (folder: VupFolderDetails): React.ReactNode => (
    <VupTableFolder
      key={folder.name}
      folder={folder}
      onRenameFolder={editFolderName}
      onDeleteFolder={deleteFolder}
      onDeleteImage={deleteImage}
      onAddImage={addImage}
      onRenameImage={editImageName}
    />
  );

  const renderList = (): React.ReactNode =>
    Object.keys(folders)
      .sort((a, b) => a.localeCompare(b))
      .filter((folderName) => !!size(folders[folderName].images))
      .map((folderKey) => renderFolder(folders[folderKey]));

  return (
    <div className={style.vupTable}>
      <div className={style.vupTableWrapper}>
        <div className={style.vupTableHeader}>
          <div className={style.vupTableHeaderWrapper}>
            <div className={style.vupTableCol1}>{columnTitles?.content || defaultTableColumnTitles.content}</div>
            <div className={style.vupTableCol2}>{columnTitles?.count || defaultTableColumnTitles.count}</div>
            <div className={style.vupTableCol3}>{columnTitles?.status || defaultTableColumnTitles.status}</div>
          </div>
        </div>
        <div className={style.vupTableContent}>{renderList()}</div>
      </div>
      <div className={style.vupTableFooter}>{renderFooter}</div>
    </div>
  );
};
