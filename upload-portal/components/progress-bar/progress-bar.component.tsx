import React from 'react';
import { CFC } from '@source/uikit';
import style from './progress-bar.component.scss';

interface ProgressBarProps {
  value: number;
}

export const ProgressBar: CFC<ProgressBarProps> = (props) => {
  return (
    <div className={style.progressBar}>
      <div className={style.progressBarValue} style={{ width: `${props.value}%` }} />
    </div>
  );
};
