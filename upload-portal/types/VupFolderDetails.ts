import { VupImageState } from '@source/modules/vendor-upload-portal/types/VupImageState';

export interface VupFolderDetails {
  errors: string[];
  images: VupImageState;
  isValid: boolean;
  name: string;
  path: string;
  previewURL: string;
  uploading: boolean;
  skippedSpinsCount: number;
  uploadingImagesProgress: number;
  uploadedImagesCount: number;
  uploadedSpinsCount: number;
}
