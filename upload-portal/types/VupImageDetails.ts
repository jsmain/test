import { RequestState } from '@source/types/enums/RequestState';
import { VupFolderImageErrorType } from '@source/modules/vendor-upload-portal/types/VupFolderImageErrorType';

export interface VupImageDetails {
  name: string;
  path: string;
  file: File;
  width: number;
  height: number;
  isValid: boolean;
  previewURL: string;
  uploadingRequestState: RequestState;
  uploadingErrorMessage: string;
  errors: { type: VupFolderImageErrorType; message: string }[];
}
