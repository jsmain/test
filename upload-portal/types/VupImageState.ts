import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';

export interface VupImageState {
  [name: string]: VupImageDetails;
}
