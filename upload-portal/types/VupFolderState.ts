import { VupFolderDetails } from './VupFolderDetails';

export interface VupFolderState {
  [name: string]: VupFolderDetails;
}
