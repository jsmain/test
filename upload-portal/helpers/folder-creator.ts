import path from 'path';
import { VupImageState } from '@source/modules/vendor-upload-portal/types/VupImageState';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';

export { createFolder, getSortedImageList, populateFolderWithImages, updateFolderName };

const createFolder = (folderName: string): VupFolderDetails => {
  return {
    path: path.resolve(folderName),
    name: folderName,
    images: {},
    previewURL: '',
    isValid: false,
    errors: [],
    uploading: false,
    skippedSpinsCount: 0,
    uploadedImagesCount: 0,
    uploadedSpinsCount: 0,
    uploadingImagesProgress: 0,
  };
};

const getSortedImageList = (images: VupImageState): VupImageDetails[] => {
  return Object.values(images).sort((a, b) => a.name.localeCompare(b.name));
};

const populateFolderWithImages = (folder: VupFolderDetails, images: VupImageState): VupFolderDetails => {
  const imagesList = getSortedImageList(images);
  const [firstImage] = imagesList;
  return {
    ...folder,
    images,
    previewURL: firstImage && firstImage.previewURL,
  };
};

const updateFolderName = (folder: VupFolderDetails, folderName: string): VupFolderDetails => {
  const newFolder = {
    ...folder,
    name: folderName,
    path: path.resolve(folderName),
  };
  return populateFolderWithImages(newFolder, folder.images);
};
