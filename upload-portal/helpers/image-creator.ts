import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import path from 'path';
import { RequestState } from '@source/types/enums/RequestState';

export { getObjectURL, loadImage, imageCreator, imagesCreator };

const imageCreator = async (file: File, folderName: string): Promise<VupImageDetails | undefined> => {
  const previewURL = getObjectURL(file);
  let image = { width: 0, height: 0 };
  try {
    image = await loadImage(previewURL);
  } catch (e) {
    return undefined;
  }

  return {
    name: file.name,
    path: path.resolve(folderName, file.name),
    errors: [],
    file: file,
    width: image.width,
    height: image.height,
    isValid: false,
    previewURL,
    uploadingRequestState: RequestState.none,
    uploadingErrorMessage: '',
  };
};

const getObjectURL = (file: File) => {
  return (URL || webkitURL).createObjectURL(file);
};

const loadImage = async (url: string): Promise<{ width: number; height: number }> => {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.addEventListener('load', function () {
      resolve({
        width: this.width,
        height: this.height,
      });
    });
    img.addEventListener('error', reject);
    img.src = url;
  });
};

const imagesCreator = async (newImages, folderName: string) => {
  const images = {};
  for (let i = 0; i < newImages.length; i++) {
    const folderImage = newImages[i];
    const image = await imageCreator(folderImage, folderName);
    if (image) {
      images[folderImage.name] = image;
    }
  }
  return images;
};
