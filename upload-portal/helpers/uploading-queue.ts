const queue: { requestPromises: any; folderName: string; cb: (folderName: string) => void }[] = [];
let isUploading = false;

export const uploadingQueue = async (
  requestPromises?: (() => Promise<any>)[],
  folderName?: string,
  cb?: (folderName: string) => void,
) => {
  if (isUploading) {
    queue.push({ requestPromises, folderName, cb });
  } else {
    isUploading = true;
    await Promise.all(requestPromises.map((callPromise) => callPromise()));
    isUploading = false;
    if (typeof cb === 'function') {
      cb(folderName);
    }
    if (queue.length) {
      const next = queue.shift();
      uploadingQueue(next.requestPromises, next.folderName, next.cb);
    }
  }
};
