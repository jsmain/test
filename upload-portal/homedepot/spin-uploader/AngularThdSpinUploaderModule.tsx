import React, { Suspense } from 'react';
import { Loader } from '@source/components/base/Loader';
import { ConfigProvider } from '@source/contexts/ConfigProvider';
import { AngularThdSpinUploader } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/AngularThdSpinUploader';

export const AngularThdSpinUploaderModule: React.FC = () => {
  return (
    <ConfigProvider>
      <Suspense fallback={<Loader />}>
        <AngularThdSpinUploader />
      </Suspense>
    </ConfigProvider>
  );
};
