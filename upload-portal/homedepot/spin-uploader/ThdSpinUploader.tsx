import React, { useEffect } from 'react';
import { noop } from 'lodash';
import {
  SupplierType,
  ThdSpinUploaderAuth,
} from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/auth/ThdSpinUploaderAuth';
import { ThdUploader } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/uploader/ThdUploader';
import { HomedepotSvg } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/icons/homedepot-svg.component';
import { ThdLogoSvg } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/icons/thd-logo-svg.component';
import {
  authThdUploadFormState,
  UploadSpinState,
  useUploadSpinState,
} from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/hooks/useUploadSpinState';
import styles from './ThdSpinUploader.scss';
import { i18n } from '@source/i18n/i18n';

export const ThdSpinUploader: React.FC = () => {
  const logoLink = 'https://sirv.com';
  const homedepotLink = 'https://homedepot.com';

  const state: UploadSpinState = useUploadSpinState();
  const handleOnSubmit = (data: authThdUploadFormState) => {
    if (data.supplierType === SupplierType.vendorSupplier) {
      return state.setAuthFormState({ ...data, workfrontId: null });
    }
    state.setAuthFormState(data);
  };

  useEffect(() => {
    if (state.authFormState.email) {
      window.history.pushState(null, window.document.title, window.location.href);
      window.addEventListener('popstate', function () {
        window.history.pushState(null, window.document.title, window.location.href);
      });
    }
    return () => {
      window.removeEventListener('popstate', noop);
    };
  }, [state.authFormState]);

  const renderContent = () => {
    if (state.authFormState.email) {
      return <ThdUploader state={state} />;
    }
    return <ThdSpinUploaderAuth onSubmit={handleOnSubmit} />;
  };

  return (
    <div className={styles.thdSpinUploader}>
      <div className={styles.thdSpinUploaderHeader}>
        <a className={styles.thdSpinUploaderHeaderLogo} href={logoLink}>
          <ThdLogoSvg />
        </a>
        <h1>{i18n.t('spinUploader.headerTitle', { defaultValue: '360 Spin Upload Portal' })}</h1>
        <a className={styles.thdSpinUploaderHeaderHomedepotLogo} href={homedepotLink}>
          <HomedepotSvg />
        </a>
      </div>
      <div className={styles.thdSpinUploaderContent}>{renderContent()}</div>
    </div>
  );
};
