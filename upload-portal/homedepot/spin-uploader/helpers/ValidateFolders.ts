import { fromPairs, every, chain } from 'lodash';
import { iterateObject } from '@source/utils/iterateObject';
import { VupFolderState } from '@source/modules/vendor-upload-portal/types/VupFolderState';
import {
  isFileTypeJpeg,
  validateFolder,
} from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/helpers/ValidateFolder';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { i18n } from '@source/i18n/i18n';
import { validateImages } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/helpers/ValidateImage';
import { getPluralizeScope } from '@source/utils/getPluralizeScope';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';

export const validateFolders = (folders: VupFolderState): { [key: string]: VupFolderDetails } => {
  return iterateObject<VupFolderState>(folders, (folderKey, folder) => {
    const folderErrors = validateFolder(folder);
    const processedImages: [string, VupImageDetails][] = Object.entries(folder.images)
      .filter(([_, image]) => isFileTypeJpeg(image.file.type, image.path))
      .map(([imageName, image]: [string, VupImageDetails]) => {
        const imageErrors = validateImages(image, folder.name);
        return [
          imageName,
          {
            ...image,
            isValid: !imageErrors.length,
            errors: imageErrors,
          },
        ];
      });

    const isValidAllImages = every(processedImages, ([, image]) => image.isValid);
    const folderImageError = chain(processedImages)
      .flatMap((o) => o[1].errors)
      .groupBy('type')
      .transform((r, v, k) => {
        r.push(
          `${i18n.t(getPluralizeScope('pluralize.imageValidateErr', v.length), { count: v.length })}` +
            ` ${k}. ${v[0].message}`,
        );
      }, [])
      .value();

    const images = fromPairs(processedImages);
    const processedFolder = {
      ...folder,
      isValid: !folderErrors.length && isValidAllImages,
      errors: folderImageError.concat(folderErrors),
      images,
    };
    return { key: folderKey, value: processedFolder };
  });
};
