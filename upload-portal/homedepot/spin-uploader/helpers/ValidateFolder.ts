import { reduce, chain } from 'lodash';
import path from 'path';
import { i18n } from '@source/i18n/i18n';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import { formatText } from '@source/utils/formatText';

const JPEG_REGEXP = /\.(jpe?g)$/i;

export const isFileTypeJpeg = (type: string, filepath: string): boolean => {
  if (type) return type === 'image/jpeg';
  const extname = path.extname(filepath);
  return JPEG_REGEXP.test(extname);
};

const folderValidators = {
  spinName: {
    validate: (spin: VupFolderDetails) => {
      const reNineDigits = /^\d{9}$/;
      const valid = reNineDigits.test(spin.name);
      return {
        valid,
        errors: !valid ? [i18n.t('spinUploader.validation.spinNamedFormat')] : null,
      };
    },
  },

  imageCount: {
    validate: (spin: VupFolderDetails) => {
      const spinNameRegExp = new RegExp('^' + spin.name + '_(S[0-9]{2}).*$');
      const invalidSpins = chain(spin.images)
        .groupBy((o) => o.name.replace(spinNameRegExp, '$1'))
        .pickBy((_, k) => /^S\d{2}$/.test(k))
        .reduce((result, v, k) => {
          const valid = chain(v)
            .groupBy((o) => o.name.replace(/_C\d{2}\.[Jj][Pp][Gg]$/, ''))
            .every((o) => [7, 13, 24].includes(o.length))
            .value();

          if (!valid) {
            result.push(k);
          }

          return result;
        }, [])
        .sortBy()
        .value();

      return {
        valid: !invalidSpins.length,
        errors:
          invalidSpins.length &&
          invalidSpins.map((num) => formatText('spinUploader.validation.folderDegreeSpin', { num })),
      };
    },
  },
};

export const validateFolder = (folder: VupFolderDetails): string[] => {
  return reduce(
    folderValidators,
    (errors, validator) => {
      const result = validator.validate(folder);
      if (!result.valid) {
        errors.push(...result.errors);
      }
      return errors;
    },
    [],
  );
};
