import { reduce } from 'lodash';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { VupFolderImageErrorType } from '@source/modules/vendor-upload-portal/types/VupFolderImageErrorType';
import { i18n } from '@source/i18n/i18n';

const imageMinWidth = 1000;
const JPG_REGEXP = /\.jpg$/i;

const validator = {
  imageName: {
    validate: (folderName: string, image: VupImageDetails) => {
      const reDigestLength = new RegExp('^' + folderName + '_S[0-9]{2}_R[0-9]{2}_C[0-9]{2}$');
      return reDigestLength.test(image.name.split('.')[0]);
    },
    error: {
      type: 'name',
      message: i18n.t('spinUploader.validation.imageNameFormat'),
    },
  },

  imageSize: {
    validate: (_, image: VupImageDetails) => {
      return !(image.width !== image.height || image.width < imageMinWidth || image.height < imageMinWidth);
    },
    error: {
      type: 'dimension',
      message: `${i18n.t('spinUploader.validation.imageSquareFormat')}${imageMinWidth} x ${imageMinWidth}.`,
    },
  },

  imageFormat: {
    validate: (_, image: VupImageDetails) => {
      return JPG_REGEXP.test(image.name);
    },
    error: {
      type: 'format',
      message: i18n.t('spinUploader.validation.imageFormat'),
    },
  },
};

export const validateImages = (
  image: VupImageDetails,
  folderName: string,
): { type: VupFolderImageErrorType; message: string }[] => {
  return reduce(
    validator,
    (errors, validator) => {
      const valid = validator.validate(folderName, image);
      if (!valid) {
        errors.push(validator.error);
      }
      return errors;
    },
    [],
  );
};
