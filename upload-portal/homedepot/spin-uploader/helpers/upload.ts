import { chain } from 'lodash';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { HomedepotService } from '@source/api/services/homedepot-service';
import { iterateObject } from '@source/utils/iterateObject';
import { i18n } from '@source/i18n/i18n';

const groupOMSIDImages = (imageList) => {
  const groupedSpins = chain(imageList)
    .groupBy((image) => {
      const spinNameRegExp = /_R\d{2}_C\d{2}\.(jpg|JPG)$/;
      const name = image.name.replace(spinNameRegExp, '');
      return `${name}.spin`;
    })
    .value();
  return iterateObject(groupedSpins, (key, value) => {
    return {
      key,
      value: value.sort((a, b) => a.name.localeCompare(b.name)),
    };
  });
};

const generateNextSpinName = (spinName: string) => {
  const SPIN_NAME_REGEXP = /^([0-9]{9})_S([0-9]{2}).spin$/;
  const match = spinName.match(SPIN_NAME_REGEXP);
  if (!match) return null;
  const [, id, number] = match;
  const nextNumber = (parseInt(number) + 1).toString().padStart(2, '0');
  return `${id}_S${nextNumber}.spin`;
};

const fetchExistingSpins = async (spinOMSID: string) => {
  const responseExistingSpins = await HomedepotService.fetchExistingSpins({ spinOMSID });
  return responseExistingSpins.data;
};

const changeSpinName = (
  existingSpins: string[],
  images: VupImageDetails[],
): { nextSpinName: string; images: VupImageDetails[] } => {
  const [largestSpinName] = existingSpins;
  const nextSpinName = generateNextSpinName(largestSpinName);
  const SPIN_AUTO_ADD_REGEXP = /([0-9]{9}_S[0-9]{2})/;
  return {
    nextSpinName,
    images: images.map((image) => ({
      ...image,
      path: image.path.replace(SPIN_AUTO_ADD_REGEXP, nextSpinName.split('.')[0]),
    })),
  };
};

const getUploadErrorMessage = (errorCode: number): string => {
  if (errorCode === 413) {
    return i18n.t('spinUploader.error.fileTooLarge', { defaultValue: 'File is too large.' });
  }
  if (errorCode !== 200) {
    return i18n.t('spinUploader.error.fileNotUploaded', { defaultValue: 'File not uploaded due to an error.' });
  }
  return '';
};

export { groupOMSIDImages, generateNextSpinName, fetchExistingSpins, changeSpinName, getUploadErrorMessage };
