import { Button, FormFieldError, Label, Select } from '@source/uikit';
import { TextField } from '@source/uikit';
import { i18n } from '@source/i18n/i18n';
import styles from './ThdSpinUploaderAuth.scss';
import { i18nButtonName } from '@source/utils/i18nButtonName';
import { Controller, useForm } from 'react-hook-form';
import { useYupValidationResolver } from '@source/hooks/yup/useYupValidationResolver';
import * as yup from 'yup';
import { EMAIL_REGEXP } from '@source/constants/email-regexp';
import { authThdUploadFormState } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/hooks/useUploadSpinState';

export enum SupplierType {
  vendorSupplier = 'Vendor/Supplier',
  creativeStudio = 'CreativeStudio',
}

const SelectOptions = [
  {
    label: 'Vendor/Supplier',
    value: SupplierType.vendorSupplier,
  },
  {
    label: 'Creative Studio',
    value: SupplierType.creativeStudio,
  },
];

interface ThdSpinUploaderAuthProps {
  onSubmit: (props: authThdUploadFormState) => void;
}

export const ThdSpinUploaderAuth: React.FC<ThdSpinUploaderAuthProps> = ({ onSubmit }) => {
  const schema = yup.object().shape({
    email: yup.string().label('email').matches(EMAIL_REGEXP, i18n.t('errorMessages.validEmail')),
    brandAdvocateEmail: yup
      .string()
      .label('brandAdvocateEmail')
      .test('brandAdvocateEmail', i18n.t('errorMessages.validEmail'), (email) => !email || !!email.match(EMAIL_REGEXP)),
    supplierType: yup.string().label('supplierType').required(i18n.t('spinUploader.supplierTypeRequired')),
    workfrontId: yup.string().when('supplierType', {
      is: (value) => value === SupplierType.creativeStudio,
      then: yup.string().label('workfrontId').required(i18n.t('spinUploader.workfrontIdRequired')),
    }),
  });

  const { handleSubmit, formState, control, watch } = useForm({
    resolver: useYupValidationResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onChange',
    defaultValues: {
      email: '',
      brandAdvocateEmail: '',
      supplierType: '',
      workfrontId: '',
    },
  });

  return (
    <div className={styles.thdAuth}>
      <h2>{i18n.t('spinUploader.welcomeBack')}</h2>
      <div className={styles.thdAuthContentWrapper}>
        <div className={styles.thdAuthContent}>
          <form onSubmit={handleSubmit((data) => onSubmit(data))}>
            <div className={styles.thdAuthForm}>
              <Label>{i18n.t('spinUploader.enterEmail')}</Label>
              <Controller
                control={control}
                name="email"
                render={({ field: { onChange, value } }) => (
                  <>
                    <TextField
                      autoFocus
                      placeholder={i18n.t('spinUploader.yourEmail')}
                      value={value}
                      onChange={onChange}
                    />
                    <FormFieldError fieldName="email" formState={formState} />
                  </>
                )}
              />
            </div>
            <div className={styles.thdAuthForm}>
              <Label>{i18n.t('spinUploader.enterBrandAdvocateEmail')}</Label>
              <Controller
                control={control}
                name="brandAdvocateEmail"
                render={({ field: { onChange, value } }) => (
                  <>
                    <TextField
                      placeholder={i18n.t('spinUploader.brandAdvocateEmail')}
                      value={value}
                      onChange={onChange}
                    />
                    <FormFieldError fieldName="brandAdvocateEmail" formState={formState} />
                  </>
                )}
              />
            </div>
            <div className={styles.thdAuthForm}>
              <Label>{i18n.t('spinUploader.selectLabel')}</Label>
              <Controller
                control={control}
                name="supplierType"
                render={({ field: { onChange, value } }) => (
                  <>
                    <Select
                      value={value}
                      options={SelectOptions}
                      onChange={onChange}
                      placeholder={i18n.t('spinUploader.selectPlaceholder')}
                    />
                    <FormFieldError fieldName="supplierType" formState={formState} />
                  </>
                )}
              />
            </div>
            {watch('supplierType') === SupplierType.creativeStudio && (
              <div className={styles.thdAuthForm}>
                <Label>{i18n.t('spinUploader.workfrontIdLabel')}</Label>
                <Controller
                  control={control}
                  name="workfrontId"
                  render={({ field: { onChange, value } }) => (
                    <>
                      <TextField
                        placeholder={i18n.t('spinUploader.workfrontIdPlaceholder')}
                        value={value}
                        onChange={(value) => onChange(value.replace(/[^0-9]/g, ''))}
                      />
                      <FormFieldError fieldName="workfrontId" formState={formState} />
                    </>
                  )}
                />
              </div>
            )}
            <Button type="submit" fullWidth={true}>
              {i18nButtonName('logIn')}
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
};
