import { Loader } from '@source/components/base/Loader';
import { RequestState } from '@source/types/enums/RequestState';
import React, { useContext, useEffect } from 'react';
import { ThdSpinUploader } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/ThdSpinUploader';
import { ConfigContext } from '@source/contexts/ConfigProvider';
import { useConfigState } from '@source/hooks/config/useConfigState';

export const AngularThdSpinUploader: React.FC = () => {
  const [config, setConfig] = useContext(ConfigContext);

  useConfigState({ config, setConfig });

  useEffect(() => {
    setConfig({ ...config, requestStatus: { state: RequestState.init } });
  }, []);

  return (
    <Loader inProgress={config.requestStatus.state !== RequestState.success}>
      <ThdSpinUploader />
    </Loader>
  );
};
