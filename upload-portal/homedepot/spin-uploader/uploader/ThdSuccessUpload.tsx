import React from 'react';
import { CFC, CheckCircleSvg } from '@source/uikit';
import style from './ThdSuccessUpload.scss';
import { UploadSpinState } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/hooks/useUploadSpinState';
import { i18n } from '@source/i18n/i18n';
import { getPluralizeScope } from '@source/utils/getPluralizeScope';

interface ThdSuccessUploadProps {
  state: UploadSpinState;
}
export const ThdSuccessUpload: CFC<ThdSuccessUploadProps> = (props) => {
  const getStats = () => {
    const result = {
      uploaded: {
        omsid: 0,
        spins: 0,
      },
      skipped: {
        omsid: 0,
        spins: 0,
      },
    };
    Object.entries(props.state.folders).forEach(([, folder]) => {
      if (folder.uploadedSpinsCount) {
        result.uploaded.spins += folder.uploadedSpinsCount;
        result.uploaded.omsid += 1;
      }
      if (folder.skippedSpinsCount) {
        result.skipped.spins += folder.skippedSpinsCount;
        result.skipped.omsid += 1;
      }
    });
    return result;
  };
  const { uploaded, skipped } = getStats();

  return (
    <div className={style.thdSuccessUpload}>
      <div className={style.thdSuccessUploadContent}>
        <div className={style.thdSuccessUploadIcon}>
          <CheckCircleSvg />
        </div>
        <div className={style.thdSuccessUploadMsg}>{i18n.t('spinUploader.uploadComplete')}</div>
        <div className={style.thdSuccessUploadStats}>
          <div className={style.thdSuccessUploadStatsRow}>
            <div className={style.thdSuccessUploadStatsRowTitle}>{i18n.t('spinUploader.uploaded')}:</div>
            <div className={style.thdSuccessUploadStatsRowDetails}>
              {i18n.t(getPluralizeScope('pluralize.spin', uploaded.spins), {
                count: uploaded.spins,
              })}{' '}
              {!!uploaded.spins &&
                i18n.t(getPluralizeScope('pluralize.uploadedSpinInOmsid', uploaded.omsid), {
                  count: uploaded.spins,
                })}
            </div>
          </div>
          <div className={style.thdSuccessUploadStatsRow}>
            <div className={style.thdSuccessUploadStatsRowTitle}>{i18n.t('spinUploader.skipped')}:</div>
            <div className={style.thdSuccessUploadStatsRowDetails}>
              {i18n.t(getPluralizeScope('pluralize.spin', skipped.spins), {
                count: skipped.spins,
              })}{' '}
              {!!skipped.spins &&
                i18n.t(getPluralizeScope('pluralize.uploadedSpinInOmsid', uploaded.omsid), {
                  count: skipped.omsid,
                })}
            </div>
          </div>
        </div>
        <div className={style.thdSuccessUploadContinue}>
          {i18n.t('spinUploader.uploadMore')},
          <div className={style.thdSuccessUploadContinueNew} onClick={props.state.reset}>
            &nbsp; {i18n.t('spinUploader.startNewSession')}.
          </div>
        </div>
      </div>
    </div>
  );
};
