import { i18n } from '@source/i18n/i18n';
import style from './ThdUploaderGuide.scss';
import { formatText } from '@source/utils/formatText';
import { CFC } from '@source/uikit';

export const ThdUploaderGuide: CFC = () => {
  const homedepotVendorLink =
    'https://homedepot.sirv.com/vendors/360%20SPIN%20IMAGE%20STANDARDS%20-%20VENDOR%20SUBMISSIONS%20-%202020.pdf';
  const portalTutorialVideoLink =
    'https://players.brightcove.net/6154186126001/bjpDO7ETg_default/index.html?videoId=6156132285001';

  const link = '<a href="https://homedepot.com" target="_blank" rel="noreferrer noopener">homedepot.com</a>';

  const supportEmail = '360spinsupport@zendesk.homedepot.com';

  return (
    <div className={style.thdUploaderGuide}>
      <div className={style.thdUploaderGuideWrapper}>
        <ul>
          <li>{i18n.t('spinUploader.guide.imagesShouldBeNamed')}</li>
          <li>{i18n.t('spinUploader.guide.eachSpinMustBeUploaded')}</li>
          <li>{formatText('spinUploader.guide.ifYouUnsure', { link })}</li>
          <li>{formatText('spinUploader.guide.canUploadSpin', { link })}</li>
          <li>{formatText('spinUploader.guide.contactToBrandAdvocate', { link })}</li>
          <li>{i18n.t('spinUploader.guide.formatMustBe')}</li>
          <li>{i18n.t('spinUploader.guide.imagesMustBeInSquare')}</li>
          <li>{i18n.t('spinUploader.guide.imageWidth')}</li>
          <li>{i18n.t('spinUploader.guide.imageAxis')}</li>
          <li>{i18n.t('spinUploader.guide.image360Degree')}</li>
          <li>{i18n.t('spinUploader.guide.image180Degree')}</li>
          <li>{i18n.t('spinUploader.guide.maxSpins')}</li>
          <li>{formatText('spinUploader.guide.spinWillAppear', { link })}</li>
        </ul>
        <p>
          {i18n.t('spinUploader.guide.forInformationReview')}
          <a
            href={homedepotVendorLink}
            onClick={() => {
              window.open(homedepotVendorLink, '_blank', 'noreferrer,noopener');
              return false;
            }}
            target="_blank"
            rel="noreferrer noopener"
          >
            {i18n.t('spinUploader.guide.imageStandards')}
          </a>
          {i18n.t('spinUploader.guide.watchOur')}
          <a href={portalTutorialVideoLink} target="_blank" rel="noreferrer noopener">
            {i18n.t('spinUploader.guide.tutorialVideo')}
          </a>
          .
        </p>
        <p>
          {i18n.t('spinUploader.guide.needHelp.question')}
          <a href={`mailto:${supportEmail}?subject=${i18n.t('spinUploader.guide.needHelp.emailSubj')}`}>{i18n.t('spinUploader.guide.needHelp.linkText')}</a>
          .
        </p>
      </div>
    </div>
  );
};
