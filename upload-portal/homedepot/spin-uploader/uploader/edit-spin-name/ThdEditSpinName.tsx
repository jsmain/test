import React, { useContext } from 'react';
import { CheckSpinNameActionType } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/types/check-spin-name-action-type';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { Button } from '@source/uikit';
import { ModalWindow } from '@source/uikit';
import style from './ThdEditSpinName.scss';
import { i18n } from '@source/i18n/i18n';
import { i18nButtonName } from '@source/utils/i18nButtonName';
import { formatText } from '@source/utils/formatText';
import { ConfigContext } from '@source/contexts/ConfigProvider';
import {
  THD_VUP_ACCOUNT_DEV,
  THD_VUP_ACCOUNT_PROD,
} from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/constants';

interface ThdEditSpinNameProps {
  image: VupImageDetails | null;
  onChangeName: (isChange: CheckSpinNameActionType) => void;
}

const splittingName = (name: string): string[] | [] => {
  return name ? name.split('_') : [];
};

export const ThdEditSpinName: React.FC<ThdEditSpinNameProps> = ({ image, onChangeName }) => {
  const [config] = useContext(ConfigContext);

  const getExistSpinPath = () => {
    if (image) {
      const THD_VUP_ACCOUNT = config?.production ? THD_VUP_ACCOUNT_PROD : THD_VUP_ACCOUNT_DEV;

      const splitName = splittingName(image?.name);
      const pathFolder = `/spin/${splitName[0].substring(splitName[0].length - 2)}`;

      return `//${THD_VUP_ACCOUNT}.${config?.sirv?.servers?.files}${pathFolder}${image?.path}?w=500&scale.option=noup`;
    } else {
      return '';
    }
  };

  const renderPopupTitle = () => {
    const splitName = splittingName(image?.name);
    return formatText('spinUploader.spinExist', { name: `${splitName[0]}_${splitName[1]}` });
  };

  const handleClose = () => null;

  return (
    <ModalWindow isOpen={!!image} onClose={handleClose}>
      <div className={style.thdEditSpinName}>
        <div className={style.thdEditSpinNameHeader}>{renderPopupTitle()}</div>
        <div className={style.thdEditSpinNameContent}>
          <div className={style.thdEditSpinNameImages}>
            <div className={style.thdEditSpinNameImagesBlock}>
              <div className={style.thdEditSpinNameImagesBlockHeader}>{i18n.t('spinUploader.existingSpin')}</div>
              <img src={getExistSpinPath()} alt={i18n.t('spinUploader.existingSpin')} />
            </div>
            <div className={style.thdEditSpinNameImagesBlock}>
              <div className={style.thdEditSpinNameImagesBlockHeader}>{i18n.t('spinUploader.newSpin')}</div>
              <img src={image?.previewURL} alt={i18n.t('spinUploader.newSpin')} />
            </div>
          </div>
          <div className={style.thdEditSpinNameButtons}>
            <div className={style.thdEditSpinNameButtonsAdd}>
              <Button onClick={() => onChangeName('change')}>{i18nButtonName('addAsAdditionalSpin')}</Button>
            </div>
            <Button variant="outline-secondary" onClick={() => onChangeName('skip')}>
              {i18nButtonName('skip')}
            </Button>
            <Button onClick={() => onChangeName('replace')}>{i18nButtonName('replace')}</Button>
          </div>
        </div>
      </div>
    </ModalWindow>
  );
};
