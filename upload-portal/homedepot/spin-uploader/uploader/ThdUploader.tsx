import React, { useMemo } from 'react';
import { chain, values, size } from 'lodash';
import { Button, PlusSvg, UploadDirButton } from '@source/uikit';
import { FolderUploadSvg } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/icons/folder-upload-svg.component';
import { ThdUploaderGuide } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/uploader/uploader-guide/ThdUploaderGuide';
import { DragFolder, FolderList } from '@source/components/drag-folder/drag-folder.component';
import { VupTable } from '@source/modules/vendor-upload-portal/components/table/VupTable';
import { UploadSpinState } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/hooks/useUploadSpinState';
import { ThdEditSpinName } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/uploader/edit-spin-name/ThdEditSpinName';
import style from './ThdUploader.scss';
import { i18n } from '@source/i18n/i18n';
import { ThdSuccessUpload } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/uploader/ThdSuccessUpload';
import { RequestState } from '@source/types/enums/RequestState';
import { i18nButtonName } from '@source/utils/i18nButtonName';

interface ThdUploaderProps {
  state: UploadSpinState;
}

export const ThdUploader: React.FC<ThdUploaderProps> = ({ state }) => {
  const hasFolders = !!size(state.folders);

  const handleOnDropFolder = (folders: FolderList) => {
    state.updateFolderList(folders);
  };

  const handleClickButton = () => {
    state.upload(navigator.onLine ? retry : true);
  };

  const retry = useMemo(
    () =>
      Object.entries(state.folders).some(([, folder]) =>
        chain(folder.images)
          .some((image) => image.uploadingRequestState === RequestState.failure)
          .value(),
      ),
    [state.folders],
  );

  const isValidFolders = useMemo(() => !Object.entries(state.folders).some(([, folder]) => !folder.isValid), [
    state.folders,
  ]);

  const isUploading = useMemo(() => Object.entries(state.folders).some(([, folder]) => folder.uploading), [
    state.folders,
  ]);

  const validateFilePath = (e: FileSystemFileEntry): boolean => {
    return /^\/[^\/\\]+[\/\\]+[^\/\\]+\.jpe?g$/i.test(e.fullPath);
  };

  const sliceFiles = (files: File[]) => {
    if (files.length) {
      const slicedFiles = files.filter((item: File) =>
        item.webkitRelativePath.match(/^[^\/\\]+[\/\\][^\/\\]+\.jpe?g$/i),
      );
      if (slicedFiles.length) {
        return slicedFiles;
      }
      return false;
    }
  };

  const handleUploadDir = (fileList: FileList) => {
    const files = values(fileList);
    const slicedFiles = sliceFiles(files);
    slicedFiles && state.addFolder(slicedFiles);
  };

  const renderFooter = () => {
    if (isUploading && !retry) {
      return <div className={style.thdUploaderFooterUpload}>{i18n.t('spinUploader.uploadingSpins')}...</div>;
    }
    return (
      <>
        <div className={style.thdUploaderFooterAdd}>
          {!retry && (
            <UploadDirButton onChangeDir={handleUploadDir}>
              <div className={style.thdUploaderFooterAddButton}>
                <PlusSvg />
                <span>{i18n.t('spinUploader.addSpin')}</span>
              </div>
            </UploadDirButton>
          )}
        </div>
        <Button variant="outline-secondary" onClick={state.reset}>
          {i18nButtonName('cancel')}
        </Button>
        <Button variant="primary" onClick={handleClickButton} isDisabled={!isValidFolders}>
          {retry ? i18nButtonName('retry') : i18nButtonName('upload')}
        </Button>
      </>
    );
  };

  const renderContent = () => {
    if (hasFolders) {
      return (
        <VupTable
          folders={state.folders}
          editFolderName={state.editFolderName}
          deleteFolder={state.deleteFolder}
          deleteImage={state.deleteImage}
          addImage={state.addImage}
          editImageName={state.editImageName}
          renderFooter={renderFooter()}
        />
      );
    }
    return (
      <div className={style.thdUploaderContentWrapper}>
        <h2>{i18n.t('spinUploader.welcomeHomeDepot')}</h2>
        <div className={style.thdUploaderContentUploadIcon}>
          <FolderUploadSvg />
        </div>
        <p>{i18n.t('spinUploader.dragFolders')}</p>
      </div>
    );
  };

  if (state.uploadComplete) {
    return <ThdSuccessUpload state={state} />;
  }

  return (
    <DragFolder onDropFolder={handleOnDropFolder} onCheckFileEntry={validateFilePath}>
      <div className={style.thdUploader}>
        <div className={style.thdUploaderContent}>{renderContent()}</div>
        <ThdUploaderGuide />
      </div>
      <ThdEditSpinName image={state.spinForEditing} onChangeName={state.actionWithSpinName} />
    </DragFolder>
  );
};
