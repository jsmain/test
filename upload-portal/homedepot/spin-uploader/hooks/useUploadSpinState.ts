import { useEffect, useRef, useState } from 'react';
import path from 'path';
import { fromPairs, identity, keys, orderBy, size, some, chain } from 'lodash';
import { FolderList } from '@source/components/drag-folder/drag-folder.component';
import { VupFolderDetails } from '@source/modules/vendor-upload-portal/types/VupFolderDetails';
import { VupImageDetails } from '@source/modules/vendor-upload-portal/types/VupImageDetails';
import { VupFolderState } from '@source/modules/vendor-upload-portal/types/VupFolderState';
import {
  createFolder,
  populateFolderWithImages,
  updateFolderName,
} from '@source/modules/vendor-upload-portal/helpers/folder-creator';
import { imagesCreator } from '@source/modules/vendor-upload-portal/helpers/image-creator';
import { validateFolders } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/helpers/ValidateFolders';
import { CheckSpinNameActionType } from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/types/check-spin-name-action-type';
import {
  changeSpinName,
  fetchExistingSpins,
  getUploadErrorMessage,
  groupOMSIDImages,
} from '@source/modules/vendor-upload-portal/homedepot/spin-uploader/helpers/upload';
import { HomedepotService } from '@source/api/services/homedepot-service';
import { RequestState } from '@source/types/enums/RequestState';
import { uploadingQueue } from '@source/modules/vendor-upload-portal/helpers/uploading-queue';
import { iterateObject } from '@source/utils/iterateObject';

// Represents upload progress in percent
const completeUploadingProgress = 100;

interface NewDirFirstFile extends File {
  webkitRelativePath?: string;
}

export type authThdUploadFormState = {
  email: string;
  brandAdvocateEmail: string;
  supplierType: string;
  workfrontId: number;
};
export interface UploadSpinState {
  folders: VupFolderState;
  authFormState: authThdUploadFormState;
  updateFolderList: (folders: FolderList) => void;
  editFolderName: (newName: string, oldName: string) => void;
  deleteFolder: (name: string) => void;
  deleteImage: (imageName: string, folderName: string) => void;
  setAuthFormState: (data: authThdUploadFormState) => void;
  editImageName: (newImageName: string, imageName: string, folderName: string) => void;
  addImage: (fileList: FileList, folderName: string) => void;
  addFolder: (files: FileList | File[]) => void;
  upload: (retry?: boolean) => void;
  spinForEditing: VupImageDetails | null;
  actionWithSpinName: (action: CheckSpinNameActionType) => void;
  uploadComplete: boolean;
  reset: () => void;
}

export function useUploadSpinState(): UploadSpinState {
  const continueUpload = useRef(null);
  const [authFormState, setAuthFormState] = useState<authThdUploadFormState>({});
  const [folders, setFolders] = useState<VupFolderState>({});
  const [spinForEditing, setSpinNameForReplace] = useState<VupImageDetails | null>(null);
  const [uploadComplete, setUploadComplete] = useState(false);
  const [uploadSpin, setUploadSpin] = useState<boolean>(false);

  useEffect(() => {
    if (uploadComplete && uploadSpin) {
      sendUploadStats();
    }
  }, [uploadComplete]);

  const handleSetFolders = (folders: VupFolderState) => {
    const validFolders = validateFolders(folders);
    setFolders(validFolders);
  };

  const processFolder = async (folderName, folderImages): Promise<VupFolderDetails> => {
    const folder = createFolder(folderName);
    const images = await imagesCreator(folderImages, folderName);
    return populateFolderWithImages(folder, images);
  };

  const updateFolderList = async (newFolders) => {
    const currentFolders = { ...folders };
    const stateFolders = {};
    for (const key in newFolders) {
      const folderFiles = newFolders[key];
      stateFolders[key] = await processFolder(key, folderFiles);
      handleSetFolders({ ...currentFolders, ...stateFolders });
    }
  };

  const editFolderName = (newName, oldName) => {
    const currentFolders = { ...folders };
    const folderToRename = { ...currentFolders[oldName] };
    delete currentFolders[oldName];
    const imagesEntries = Object.entries(folderToRename.images).map(([, image]: [string, VupImageDetails]) => {
      return [image.name, { ...image, path: path.resolve(newName, image.name) }];
    });
    folderToRename.images = fromPairs(imagesEntries);
    const newFolder = updateFolderName(folderToRename, newName);
    handleSetFolders({ ...currentFolders, [newName]: { ...newFolder } });
  };

  const deleteFolder = (name) => {
    const currentFolders = { ...folders };
    delete currentFolders[name];
    setFolders(currentFolders);
  };

  const deleteImage = (imageName, folderName) => {
    const folder = { ...folders[folderName] };
    delete folder.images[imageName];
    if (!!size(folder.images)) {
      handleSetFolders({ ...folders, [folderName]: folder });
    } else {
      deleteFolder(folderName);
    }
  };

  const editImageName = (newImageName, imageName, folderName) => {
    const folder = { ...folders[folderName] };
    const image = folder.images[imageName];
    image.name = newImageName;
    folder.images[newImageName] = image;
    delete folder.images[imageName];
    handleSetFolders({ ...folders, [folderName]: folder });
  };

  const addImage = async (files, folderName) => {
    const folder = { ...folders[folderName] };
    const newImages = await imagesCreator(files, folderName);
    const folderImages = { ...folder.images, ...newImages };
    const updatedFolder = populateFolderWithImages(folder, folderImages);
    handleSetFolders({ ...folders, [folderName]: updatedFolder });
  };

  const addFolder = (files) => {
    const firstFile: NewDirFirstFile = files[0];
    const dirName = firstFile.webkitRelativePath.split('/')[0];
    updateFolderList({ [dirName]: files });
  };

  const updateFolderSkipImages = (folderName: string, imageList: VupImageDetails[]) => {
    setFolders((folders) => {
      const folder = { ...folders[folderName] };
      imageList.forEach((image) => {
        delete folder.images[image.name];
      });
      folder.skippedSpinsCount += 1;
      folder.uploading = size(folder.images) ? folder.uploading : false;
      const updatedFolders = { ...folders, [folderName]: folder };
      checkCompleteUpload(updatedFolders);
      return updatedFolders;
    });
  };

  const sendUploadStats = () => {
    const uploads: { omsid: string; spins: number }[] = [];
    Object.entries(folders).forEach(([, folder]: [string, VupFolderDetails]) => {
      if (folder.uploadedSpinsCount) {
        uploads.push({ omsid: folder.name, spins: folder.uploadedSpinsCount });
      }
    });
    const requestData = {
      stats: {
        version: 2,
        uploads,
        vendor: authFormState.email,
        brandAdvocate: authFormState.brandAdvocateEmail,
      },
    };
    HomedepotService.sendUploadSpinStats(requestData);
  };

  const checkCompleteUpload = (folders) => {
    const isAllFilesUploaded = chain(folders)
      .every((folder) =>
        chain(folder.images)
          .every((image) => image.uploadingRequestState === RequestState.success)
          .value(),
      )
      .value();
    if (isAllFilesUploaded) {
      const uploadedSpin = chain(folders)
        .some((folder) => folder.uploadedSpinsCount !== 0)
        .value();
      setUploadSpin(uploadedSpin);
      setUploadComplete(true);
    }
  };

  const setUploadedSpin = (folderName) => {
    setFolders((folders) => {
      const folder = { ...folders[folderName] };
      folder.uploadedSpinsCount += 1;
      const updatedFolders = { ...folders, [folderName]: folder };
      checkCompleteUpload(updatedFolders);
      return updatedFolders;
    });
  };

  const actionWithSpinName = (action) => {
    continueUpload.current?.(action);
    continueUpload.current = null;
    setSpinNameForReplace(null);
  };

  const getActionWithDuplicateSpin = async (image): Promise<CheckSpinNameActionType> => {
    setSpinNameForReplace(image);
    return new Promise((resolve) => {
      continueUpload.current = resolve;
    });
  };

  const setUploadedImageStatus = (folderName: string, image: VupImageDetails, uploadingStatusCode) => {
    setFolders((folders) => {
      const folder = { ...folders[folderName] };
      const updatedImage = {
        ...image,
        uploadingRequestState: uploadingStatusCode === 200 ? RequestState.success : RequestState.failure,
        uploadingErrorMessage: getUploadErrorMessage(uploadingStatusCode),
      };
      const folderImages = { ...folder.images, [image.name]: updatedImage };
      const updatedFolder = populateFolderWithImages(folder, folderImages);
      if (updatedImage.uploadingRequestState === RequestState.success) {
        updatedFolder.uploadedImagesCount++;
      }
      updatedFolder.uploadingImagesProgress = Math.floor(
        (updatedFolder.uploadedImagesCount * 100) / size(folderImages),
      );
      updatedFolder.uploading = some(folderImages, (image) => image.uploadingRequestState === RequestState.request);
      return { ...folders, [folderName]: updatedFolder };
    });
  };

  const setRequestUploadImageStatus = (folderName: string, images: VupImageDetails[]) => {
    setFolders((folders) => {
      const folder = folders[folderName];
      const folderImages = { ...folder.images };
      const updatedImages = {};
      Object.entries(folderImages).map(([imageName, image]: [string, VupImageDetails]) => {
        const updatedImage = { ...image };
        if (images.find((image) => image.name === updatedImage.name)) {
          updatedImage.uploadingRequestState =
            image.uploadingRequestState === RequestState.success ? image.uploadingRequestState : RequestState.request;
        }
        updatedImages[imageName] = updatedImage;
      });
      const updatedFolder = populateFolderWithImages(folder, updatedImages);
      updatedFolder.uploading = updatedFolder.uploadingImagesProgress !== completeUploadingProgress;
      return { ...folders, [folderName]: updatedFolder };
    });
  };

  const imageUploader = (folderName: string, imagesList: VupImageDetails[], retry?: boolean) => {
    const images = retry
      ? imagesList.filter((image) => image.uploadingRequestState !== RequestState.success)
      : imagesList;
    const requestPromises = images.map((image) => async () => {
      try {
        await HomedepotService.uploadImage({
          file: image.file,
          path: image.path,
          ...authFormState,
        });
        setUploadedImageStatus(folderName, { ...image }, 200);
      } catch (e) {
        setUploadedImageStatus(folderName, { ...image }, e.response?.status || 500);
      }
    });
    setRequestUploadImageStatus(folderName, images);
    uploadingQueue(requestPromises, folderName, setUploadedSpin);
  };

  const setFolderUploadingStatus = (retry: boolean) => {
    setFolders((folders) => {
      return iterateObject<VupFolderState>(folders, (key, folder) => {
        folder.uploading = !retry;
        folder.uploadedSpinsCount = 0;
        folder.skippedSpinsCount = 0;
        return {
          key,
          value: { ...folder },
        };
      });
    });
  };

  const upload = async (retry) => {
    setFolderUploadingStatus(retry);
    const folderKeys = Object.keys(folders).sort((a, b) => a.localeCompare(b));
    for (let i = 0; i < folderKeys.length; i++) {
      const folderKey = folderKeys[i];
      const { name, images } = folders[folderKey];
      const spinsGroupedByName = groupOMSIDImages(images);
      const spinNames = orderBy(keys(spinsGroupedByName), identity, 'asc');
      try {
        const existingSpins = await fetchExistingSpins(name);
        for (let i = 0; i < spinNames.length; i++) {
          const spinName = spinNames[i];
          if (existingSpins.some((s) => s === spinName)) {
            const action = await getActionWithDuplicateSpin(spinsGroupedByName[spinName][0]);
            if (action === 'change') {
              const changeSpinNameResult = changeSpinName(existingSpins, spinsGroupedByName[spinName]);
              existingSpins.unshift(changeSpinNameResult.nextSpinName);
              spinsGroupedByName[changeSpinNameResult.nextSpinName] = changeSpinNameResult.images;
              delete spinsGroupedByName[spinName];
              imageUploader(name, changeSpinNameResult.images, retry);
            } else if (action === 'skip') {
              updateFolderSkipImages(name, spinsGroupedByName[spinName]);
              delete spinsGroupedByName[spinName];
            } else {
              imageUploader(name, spinsGroupedByName[spinName], retry);
            }
          } else {
            imageUploader(name, spinsGroupedByName[spinName], retry);
          }
        }
      } catch (e) {}
    }
  };

  const reset = () => {
    setAuthFormState({});
    setFolders({});
    setSpinNameForReplace(null);
    setUploadComplete(false);
  };

  return {
    folders,
    updateFolderList,
    setAuthFormState,
    editFolderName,
    deleteFolder,
    deleteImage,
    editImageName,
    addImage,
    addFolder,
    upload,
    spinForEditing,
    actionWithSpinName,
    uploadComplete,
    reset,
    authFormState,
  };
}
